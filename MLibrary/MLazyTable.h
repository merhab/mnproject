#pragma once
#include "MTable.h"


#include <sys/_types/_int64_t.h>
class MLazyTable {
public:
  int64_t getRecordCount() const;

  void setRecordCount(int64_t recordCount);

  int64_t getLimit() const;

  void setLimit(int64_t limit);

  int64_t getOffset() const;

  void setOffset(int64_t offset);

  const QVector<MTable *> &getTables() const;

  void setTables(const QVector<MTable *> &tables);

protected:
  int64_t _recordCount = -1;
  int64_t _limit = 0;
  int64_t _offset = 0;
  QVector<MTable *> _tables;
};
