//
// Created by MERHAB on 02/08/2023.
//
#pragma once
#include "MRecord.h"
#include "mtest.h"
inline bool testMRecord(){
    out <<
        magenta("******************************\n")<<
        magenta("*******testing MRecord********\n")<<
        magenta("******************************\n");
    bool res = true;
    auto meta = metaInt("idi").setAutoInc().setPrimary();
    auto meta2 = metaStr("name").setUnic();
    auto meta3 = metaFloat("salary");
    MRecord rd;
    rd.append({
        {&meta,10},
        {&meta2,"ali"},
        {&meta3,1.5f}
    });
    //out << $(rd);
    test($(rd)=="{meta:{name:id,type:Int64,primary:true,autoInc:true,unic:false,readOnly:false,priority:Normal,generated:true,visible:false,default val:,column:0}\n"
                ",val:-1}\n"
                "{meta:{name:idMaster,type:Int64,primary:false,autoInc:false,unic:false,readOnly:false,priority:Normal,generated:true,visible:false,default val:,column:1}\n"
                ",val:-1}\n"
                "{meta:{name:idi,type:Int,primary:true,autoInc:true,unic:false,readOnly:false,priority:Normal,generated:true,visible:true,default val:,column:}\n"
                ",val:10}\n"
                "{meta:{name:name,type:String,primary:false,autoInc:false,unic:true,readOnly:false,priority:Normal,generated:true,visible:true,default val:,column:}\n"
                ",val:ali}\n"
                "{meta:{name:salary,type:Float,primary:false,autoInc:false,unic:false,readOnly:false,priority:Normal,generated:true,visible:true,default val:,column:}\n"
                ",val:1.5}\n","rd.append()");
    test($(rd.fieldByName("name"))=="{meta:{name:name,type:String,primary:false,autoInc:false,unic:true,readOnly:false,priority:Normal,generated:true,visible:true,default val:,column:}\n"
                  ",val:ali}\n","rd.fieldByName(\"name\")");
    rd.getFieldset().goTo(3);
    //out << $(rd.getFieldset().current())<<"\n";
    test($(rd.getFieldset().current())=="{meta:{name:name,type:String,primary:false,autoInc:false,unic:true,readOnly:false,priority:Normal,generated:true,visible:true,default val:,column:}\n"
           ",val:ali}\n","rd.getFieldset().goTo(3)");
    return res;
}
