//
// Created by MERHAB on 03/08/2023.
//

#pragma once
#include "MStmt.h"
#include "mtest.h"
inline bool testMStmt(){
    bool res = true;
    MStmt s;
    s.values().append({10,"ali"});
    s.names().append({"id","name"});
    out << $(s);
    return res;
}