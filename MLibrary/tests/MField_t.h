//
// Created by MERHAB on 02/08/2023.
//
#pragma once
#include "MField.h"
#include "mtest.h"
inline bool testMField(){
    out <<
        magenta("******************************\n")<<
        magenta("********testing MField********\n")<<
        magenta("******************************\n");
    bool res = true;
    auto meta = metaStr("_str");
    MField fld = {&meta,"1"};
    res = res && test($(fld)=="{meta:{name:_str,type:String,primary:false,autoInc:false,unic:false,readOnly:false,priority:Normal,generated:true,visible:true,default val:,column:}\n"
                 ",val:1}\n","MField fld = {&meta,1}");
    //out << $(fld) <<"\n";
    fld.setValue("10");
    res = res && test($(fld)=="{meta:{name:_str,type:String,primary:false,autoInc:false,unic:false,readOnly:false,priority:Normal,generated:true,visible:true,default val:,column:}\n"
                              ",val:10}\n","fld.setValue(10)");
    meta = metaInt("mi");
    fld.setMeta(&meta);
    res = res && test($(fld)=="{meta:{name:mi,type:Int,primary:false,autoInc:false,unic:false,readOnly:false,priority:Normal,generated:true,visible:true,default val:,column:}\n"
                              ",val:10}\n","fld.setMeta(&meta)");
    return res;
}
