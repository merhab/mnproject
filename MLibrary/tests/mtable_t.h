#include "MEvent.h"
#include "MRecord.h"
#include "MTable.h"
#include "meta.h"
#include "mtest.h"
#include <qsqldatabase.h>
#include <QSqlError>
#include "MEventList.h"

 static QVariantList bMove(void* receiver,void* sender,QVariantList args){
  out << "beforeMove" <<"\n";
     return {};
}

 static QVariantList cMove(void* receiver,void* sender,QVariantList args){
  out << "can move" <<"\n";
  return {true};
}

static MEventList eventList(0,0,"test");
static void  initEvent(){
  eventList.registerEvent(MEvent(bMove,MTableBeforeMove));
    auto fptr2 = &cMove;
    eventList.registerEvent(MEvent(cMove,MTableCanMove));
}

inline bool testMTable(){
 out <<
    magenta("******************************\n")<<
    magenta("********testing MTable********\n")<<
    magenta("******************************\n");
  bool ret = true;
  QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
  db.setDatabaseName("test.db");
  if(db.open()){
    qDebug() << green("db connected") << "\n";
  }else{
      qDebug() << red("db connection error") << "\n";
  }
  MTable table(&db,"user",
	       {newMetaStr("name"),
		newMetaInt("age"),
		newMetaStr("address")});
  auto st = table.getMetaList().createTable(table.tableName());
  out <<  st.stmt << "\n";
  test("CREATE TABLE IF NOT EXISTS `user`(`id` INTEGER PRIMARY KEY AUTOINCREMENT,`idMaster` INTEGER,`name` TEXT,`age` INTEGER,`address` TEXT)"== st.stmt,"table.getMetaList().createTable(table.getTableName())");
  db.exec(st.stmt);
  ret =ret && db.lastError().type()==QSqlError::NoError;
  test(ret,"db.exec(createTable)");
  MRecord rd(table.getMetaList());
  /* rd.fieldByName(IDMaster).setValue(int64_t(-1)); */
  /* rd.fieldByName("name").setValue("noure ddine"); */
  /* rd.fieldByName("age").setValue(45); */
  /* rd.fieldByName("address").setValue("hun an"); */
  rd.setValues({int64_t(-1),"nour",45,"hunan"});
  table.insert(&rd);
  table.dataSet().append(rd);
  out << rd.getID() << "\n";
  for(int i=0;i<15;i++){
    table.append({int64_t(-1),"ali"+QString::number(i),i,"mascara"});
}
  out<< table.current().getID()<<"\n";
  table.dataSet().first();
  for(int i=0;i<15;i++){
    if(table.remove()){
        table.dataSet().remove();
    }
  }
  //table.insert();
 table.current().fieldByName("name").setValue("minouna");
  table.update();
  table.select().filter(filter("id") > 0).
  andFilter(filter("age")>0).
  andFilter(filter("name") == "nour").
  orFilter(filter("name")=="ali14").exec();
  out << "record count:" << table.dataSet().size() << "\n";
    initEvent();
    table.appendEventList(eventList);
    do {
        out << $(table.current())<<"\n";
    } while (table.next());
  return ret;
}
