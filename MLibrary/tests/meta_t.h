//
// Created by MERHAB on 02/08/2023.
//

#pragma once
#include "mtest.h"
#include "meta.h"
inline bool testMeta(){
    out <<
    magenta("******************************\n")<<
    magenta("*********testing Meta*********\n")<<
    magenta("******************************\n");
    bool res = true;
    Meta meta=metaInt("age");
    //out << $(meta);
    res = res && test($(meta) == "{name:age,type:Int,primary:false,autoInc:false,unic:false,readOnly:false,priority:Normal,generated:true,visible:true,default val:,column:}\n",
         "metaInt(name)");
    meta=metaInt64("age");
    //out << $(meta);
    res = res && test($(meta) == "{name:age,type:Int64,primary:false,autoInc:false,unic:false,readOnly:false,priority:Normal,generated:true,visible:true,default val:,column:}\n",
                      "metaInt64(name)");
    meta=metaFloat("age");
    //out << $(meta);
    res = res && test($(meta) == "{name:age,type:Float,primary:false,autoInc:false,unic:false,readOnly:false,priority:Normal,generated:true,visible:true,default val:,column:}\n",
                      "metaFloat(name)");
    meta=metaStr("age");
    //out << $(meta);
    res = res && test($(meta) == "{name:age,type:String,primary:false,autoInc:false,unic:false,readOnly:false,priority:Normal,generated:true,visible:true,default val:,column:}\n",
                      "metaStr(name)");
    meta.setAutoInc();
    res = res && test($(meta) == "{name:age,type:String,primary:false,autoInc:true,unic:false,readOnly:false,priority:Normal,generated:true,visible:true,default val:,column:}\n",
                      "meta.setAutoInc()");
    meta.setColumn(10);
    res = res && test($(meta) == "{name:age,type:String,primary:false,autoInc:true,unic:false,readOnly:false,priority:Normal,generated:true,visible:true,default val:,column:10}\n",
                      "meta.setColumn(10)");
    meta.setComplimentary();
    res = res && test($(meta) == "{name:age,type:String,primary:false,autoInc:true,unic:false,readOnly:false,priority:Complimentary,generated:true,visible:true,default val:,column:10}\n",
                      "meta.setComplimentary()");
    meta.setDefaultValue("_str");
    res = res && test($(meta) == "{name:age,type:String,primary:false,autoInc:true,unic:false,readOnly:false,priority:Complimentary,generated:true,visible:true,default val:_str,column:10}\n",
                      "meta.setDefaultValue(\"_str\")");
    meta.setNotGenerated();
    res = res && test($(meta) == "{name:age,type:String,primary:false,autoInc:true,unic:false,readOnly:false,priority:Complimentary,generated:false,visible:true,default val:_str,column:10}\n",
                      "meta.setNotGenerated()");
    meta.setGenerated();
    res = res && test($(meta) == "{name:age,type:String,primary:false,autoInc:true,unic:false,readOnly:false,priority:Complimentary,generated:true,visible:true,default val:_str,column:10}\n",
                      "meta.setGenerated()");
    return res;
}