//
// Created by mac on 03/08/2023.
//
#pragma once
#include "mdataset.h"
#include "mtest.h"


inline void beforeMove(MDataSet<int>* vec,int64_t current,int64_t next){
    out << "from "<<current<< " To : "<< next <<"\n";
}

inline bool canMove(MDataSet<int>* vec,int64_t current,int64_t next){
    if(next > 2) {
        out << red(" I Dont allow move\n");
        return false;
    }
    return true;
}
inline void afterMove(MDataSet<int>* vec,int64_t next){
    out << "we r in: "<<  next <<"\n";
}
inline bool testMdataset(){
    QVector<int> list = {1,10,7,8};
    MDataSet<int> dataSet(&list);
    dataSet.append_beforeMoveFuncs(beforeMove);
    dataSet.append_canMoveFuncs(canMove);
    dataSet.append_afterRemoveFuncs(afterMove);
    dataSet.current() = -1;
    out << dataSet.current() << "\n";
    if(dataSet.next())
        out << dataSet.current() << "\n";
    if(dataSet.last())
        out << dataSet.current() << "\n";
    if (dataSet.next()){
        out << dataSet.current() << "\n";
    }else{
        out << "ERROR\n";
    }
    if(dataSet.prior())
        out << dataSet.current() << "\n";
    return true;
}
