//
// Created by MERHAB on 26/08/2023.
//
#pragma once
#include "MDbAbleGui.h"
#include "typeDef.h"
#include "MTable.h"


class MDataSource {
public:
    const QVector<MDbAbleGui *> &getDbControls() const;

    MTable *getTable() const;

    MDataSource& setTable(MTable *table);

    MDataSource& appendGui(MDbAbleGui* gui);

private:
    MTable* _table;
    QVector<MDbAbleGui*> _dbControls;
};

QVariantList onMdbAbleGuiValidate(void* sender,void* receiver,QVariantList args);
QVariantList onMTableAfterMove(void* sender,void* receiver,QVariantList args);
