//
// Created by MERHAB on 01/08/2023.
//

#pragma once

#include "MField.h"
#include "MStmt.h"
#include "MetaList.h"
#include "mdataset.h"
#include "MAlphaNum.h"


struct MFieldBool {
  MField *fld;
  bool hasError;
};
class MRecord {
public:
  MRecord();

  MRecord(MetaList &metaList);
  MRecord(MVector<Meta*> metas);

  MField& fieldByName(MString name);

  MField& fieldByIndex(int index);

  MDataSet<MField> &getFieldset();

  void setFieldset(const MDataSet<MField> &fieldset);

   MVector<MField> &getFields() ;

  void setFields(const MVector<MField> &fields);

  int64_t size();

  MField *itemAt(int index);

  void append(MField fld);

  void append(MVector<MField> flds);

  MStmt insert(MString tableName, int64_t idMaster = -1);
  MStmt update(MString tableName);
  MStmt remove(MString tableName);
  int64_t getID();
  int64_t getIDMaster();
  MField &getIDField();
  MField &getIDMasterField();
  MField &getFirstField();
  int firstIndex();
  bool isDirty() const;

  void setDirty(bool dirty);

  bool isDeleted() const;

  void setDeleted(bool deleted);
  bool isNew();
  MString names();
  MStringList namesList();
  void setValues(MVector<MAlphaNum> vals);
protected:
  MVector<MField> fields;
  MDataSet<MField> fieldset;
  bool dirty = false;
  bool deleted = false;
};

MString $d(MRecord);
MString $(MRecord rd);
