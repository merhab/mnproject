//
// Created by MERHAB on 01/08/2023.
//

#include "MStmt.h"
#include <cassert>

MStmt::MStmt() {
  
}

MStmt::MStmt(MStmtKind kind, MString stmt ,MStringList names ,MAlphaNumList values) {
  this->kind = kind;
  this->stmt = stmt;
  this->_names = names;
  this->_values = values;
}

MStmt::MStmt(const MStmt& stmt) {
    this->_values = stmt._values;
    this->_names = stmt._names;
    this->stmt = stmt.stmt;
    this->kind = stmt.kind;
}

MStringList& MStmt::names() {
    return _names;
}

MAlphaNumList &MStmt::values() {
    return _values;
}

void MStmt::setNames(const MStringList &names) {
    _names = names;
}

void MStmt::setValues(const MAlphaNumList &values) {
    _values = values;
}

MString $(MStmt self) {
  MString s="{";
  if(self.names().size()==self.values().size()){
for(int i=0 ; i< self.names().size();i++) {
    s += self.names()[i] + ":" + self.values()[i].toString() + ",";
}
}else{
      for(int i=0 ; i< self.names().size();i++) {
          s += self.names()[i] + ",";
      }
  }
  s += "\nstmt:" + self.stmt + "\n";
  s += "kind:"+$(self.kind) +"}\n";
  return s;
}

MStmt operatorLogic(MStmt s1, MStmt &s2, const MString &logic) {
  assert(s1.stmt != "" or s2.stmt!="");
  MStmt result;
  result = s1;
  result.names().append(s2.names());
  result.stmt = "(" + result.stmt + ") " + logic + " (" + s2.stmt + ")";
  return result;
}

MStmt operator&&(MStmt s1,  MStmt &s2) {
  return operatorLogic(s1, s2, " AND ");
}


MStmt operator||(MStmt s1,  MStmt &s2) {
  return operatorLogic(s1, s2, " OR ");
}

MStmt operator!(MStmt s1) {
  MStmt result = s1;
  result.stmt = "NOT " + result.stmt;
  return result;
}

MStmt cmp(MStmt s, const MAlphaNum &val, const MString &cmpOperator) {
  assert(s.names().size() == 1);
  MStmt result = s;
  result.values().append(val);
  result.stmt = s.names()[0] + " " + cmpOperator + " ? ";
  return result;
}

MStmt operator==(MStmt s, const MAlphaNum &val) {
  return cmp(s, val, "=");
}

MStmt operator>(MStmt s, const MAlphaNum &val) {
  return cmp(s, val, ">");
}

MStmt operator>=(MStmt s, const MAlphaNum &val) {
  return cmp(s, val, ">=");
}

MStmt operator<(MStmt s, const MAlphaNum &val) {
  return cmp(s, val, "<");
}

MStmt operator<=(MStmt s, const MAlphaNum &val) {
  return cmp(s, val, "<=");
}

MStmt operator%(MStmt s, const MAlphaNum &val) {
  return cmp(s, val, "LIKE");
}

MStmt filter(MStringList names, MAlphaNumList vals) {
    return MStmt(AndFilter,"",names,vals);
}

MStmt filter(MString name) {
    return filter(MStringList().append(name),{});
}

