//
// Created by mac on 31/07/2023.
//

#include "meta.h"

Meta::Meta(const MString &name, MSqlType type, bool primary, bool autoInc,
           bool unic, bool readOnly, Priority priority, bool generated,
           bool visible, const MAlphaNum &defaultVal, MAlphaNum col)
    : name(name), type(type), primary(primary), autoInc(autoInc), unic(unic),
      readOnly(readOnly), priority(priority), generated(generated),
      visible(visible), defaultVal(defaultVal), col(col) , caption(name) {}

Meta::Meta(const Meta &meta)
    : name(meta.name), type(meta.type), primary(meta.primary),
      autoInc(meta.autoInc), unic(meta.unic), readOnly(meta.readOnly),
      priority(meta.priority), generated(meta.generated), visible(meta.visible),
      defaultVal(meta.defaultVal), col(meta.col), caption(meta.caption) {}

Meta metaInt(const MString &name) {
  return Meta(name, Int, false, false, false, false, Normal, true, true,
              MAlphaNum(), MAlphaNum());
}

Meta metaStr(const MString &name) {
  return Meta(name, String, false, false, false, false, Normal, true, true,
              MAlphaNum(), MAlphaNum());
}

Meta metaFloat(const MString &name) {
  return Meta(name, Float, false, false, false, false, Normal, true, true,
              MAlphaNum(), MAlphaNum());
}

Meta metaInt64(const MString &name) {
  return Meta(name, Int64, false, false, false,
              false, Normal, true, true,
              MAlphaNum(), MAlphaNum());
}

MString $(Meta meta) {
    MString s;
    s = "{name:"+meta.getName()+
            ",type:"+sqlTypeToStr(meta.getType())+
            ",primary:"+MAlphaNum(meta.isPrimary()).toString()+
            ",autoInc:"+MAlphaNum(meta.isAutoInc()).toString()+
            ",unic:"+MAlphaNum(meta.isUnic()).toString()+
            ",readOnly:"+MAlphaNum(meta.isReadOnly()).toString()+
            ",priority:"+ priorityToStr(meta.getPriority())+
            ",generated:"+MAlphaNum(meta.isGenerated()).toString()+
            ",visible:"+MAlphaNum(meta.isVisible()).toString()+
            ",default val:"+meta.getDefaultVal().toString()+
            ",column:"+meta.getCol().toString()+"}\n";
    return s;
}

MString sqlTypeToStr(MSqlType type) {
    switch (type) {

        case Int:
            return "Int";
            break;
        case Int64:
            return "Int64";
            break;
        case String:
            return "String";
            break;
        case Float:
            return "Float";
            break;
        default:
            assert(false);
    }
}

MString priorityToStr(Priority p) {
    switch (p) {

        case Obligatory:
            return "Obligatory";
            break;
        case Complimentary:
            return "Complimentary";
            break;
        case Normal:
            return "Normal";
            break;
        default:
            assert(false);
    }
}

Meta *newMetaStr(const MString &name) {
    return new Meta(name, String, false, false, false, false, Normal, true, true,
                       MAlphaNum(), MAlphaNum());;
}

Meta *newMetaInt(const MString &name) {
    return new Meta(name, Int, false, false, false, false, Normal, true, true,
                    MAlphaNum(), MAlphaNum());
}

Meta *newMetaInt64(const MString &name) {
    return new Meta(name, Int64, false, false, false, false, Normal, true, true,
                    MAlphaNum(), MAlphaNum());
}

Meta *newMetaFloat(const MString &name) {
    return new Meta(name, Float, false, false, false, false, Normal, true, true,
                    MAlphaNum(), MAlphaNum());
}

Meta &Meta::setPrimary(bool isPrimary) {
  primary = isPrimary;
  return *this;
}

Meta &Meta::setAutoInc(bool isAutoInc) {
  this->autoInc = isAutoInc;
  return *this;
}

Meta& Meta::setUnic(bool isUnic) {
  this->unic = isUnic;
  return *this;
}

Meta& Meta::setReadOnly(bool isReadOnly) {
  this->readOnly = isReadOnly;
  return *this;
}

Meta& Meta::setMendatory() {
  this->priority = Obligatory;
  return *this;
}

Meta& Meta::setComplimentary() {
  this->priority = Complimentary;
  return *this;
}

Meta& Meta::setNormalPriority() {
  this->priority = Normal;
  return *this;
}

Meta& Meta::setNotGenerated() {
  this->generated = false;

  return *this;
}

Meta& Meta::setGenerated() {
  this->generated =true;
  return *this;
}

Meta& Meta::setInvisible() {
  this->visible =false;

  return *this;
}

Meta& Meta::setVisible() {
  this->visible = true;
  return *this;
}

Meta& Meta::setColumn(int column) {
  this->col = column;
  return *this;
}

Meta& Meta::setDefaultValue(MAlphaNum value) {
  this->defaultVal = value;
  return *this;
}

MString Meta::getName() {
  return this->name;
}

MString Meta::getMasterFieldName() const {
    return masterFieldName;
}

void Meta::setMasterFieldName(const MString masterFieldName) {
    this->masterFieldName = masterFieldName;
}

MString Meta::getMasterTableName() const {
    return masterTableName;
}

Meta &Meta::setMasterTableName(const MString masterTableName) {
    this->masterTableName = masterTableName;
    return *this;
}

MSqlType Meta::getType() const {
    return type;
}

void Meta::setType(MSqlType type) {
    Meta::type = type;
}

bool Meta::isPrimary() const {
    return primary;
}

bool Meta::isAutoInc() const {
    return autoInc;
}

bool Meta::isUnic() const {
    return unic;
}

bool Meta::isReadOnly() const {
    return readOnly;
}

Priority Meta::getPriority() const {
    return priority;
}

bool Meta::isMondatory() {
    return this->getPriority() == Obligatory;
}


bool Meta::isGenerated() const {
    return generated;
}

bool Meta::isVisible() const {
    return visible;
}

MAlphaNum &Meta::getDefaultVal()  {
    return defaultVal;
}

 MAlphaNum &Meta::getCol()  {
    return col;
}

const MAlphaNum &Meta::getVisibleValue() const {
    return visibleValue;
}

MString Meta::getQtType() {
  MString b;
    switch (this->type) {
        case Int:
            b="int";
            break;
        case Int64:
            b="qlonglong";
            break;
        case String:
            b="MString";
            break;
        case Float:
            b="float";
            break;
        case Null:
            b="";
            break;
        default:
            assert(false);
    }
    return b;
}

MAlphaNum Meta::initVal() {
    switch (this->type) {

        case Int:
            return 0;
            break;
        case Int64:
            return -1;//I use this only for Id , if used for anything else will cause error
            break;
        case String:
            return "";
            break;
        case Float:
            return float(0);
            break;
        case Null:
            return {};
            break;
        default:
            assert(false);
    }
}

const MString &Meta::getCaption() const {
    return caption;
}

Meta& Meta::setCaption(const MString &label) {
    Meta::caption = label;
    return *this;
}

