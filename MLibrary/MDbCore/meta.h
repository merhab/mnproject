//
// Created by mac on 31/07/2023.
//

#pragma once
#include "../Core/MString.h"
#include "../Core/MAlphaNum.h"
enum MSqlType {
    Int, Int64, String, Float, Null
};

MString sqlTypeToStr(MSqlType type);

enum Priority {
    Obligatory, Complimentary, Normal
};

MString priorityToStr(Priority p);

class Meta {
public:
    MString getMasterFieldName() const;

    void setMasterFieldName(const MString masterFieldName);

    MString getMasterTableName() const;

    Meta &setMasterTableName(MString masterTableName);

    Meta(const MString &name, MSqlType type, bool primary, bool autoInc, bool unic,
         bool readOnly, Priority priority, bool generated, bool visible,
         const MAlphaNum &defaultVal, MAlphaNum col);

    Meta(const Meta &meta);

    Meta &setPrimary(bool isPrimary = true);

    Meta &setAutoInc(bool isAutoInc = true);

    Meta &setUnic(bool isUnic = true);

    Meta &setReadOnly(bool isReadOnly = true);

    Meta &setMendatory();

    Meta &setComplimentary();

    Meta &setNormalPriority();

    Meta &setNotGenerated();

    Meta &setGenerated();

    Meta &setInvisible();

    Meta &setVisible();

    Meta &setColumn(int column);

    Meta &setDefaultValue(MAlphaNum value);

    MString getName();

    MSqlType getType() const;

    void setType(MSqlType type);

    bool isPrimary() const;

    bool isAutoInc() const;

    bool isUnic() const;

    bool isReadOnly() const;

    Priority getPriority() const;

    bool isMondatory();

    bool isGenerated() const;

    bool isVisible() const;

    MAlphaNum &getDefaultVal() ;

     MAlphaNum &getCol() ;

    const MAlphaNum &getVisibleValue() const;

    MString getQtType();

    MAlphaNum initVal();

    const MString &getCaption() const;

    Meta &setCaption(const MString &label);

private:
    MString name;
    MSqlType type;
    bool primary = false;
    bool autoInc = false;
    bool unic = false;
    bool readOnly = false;
    Priority priority = Normal;
    bool generated = true;
    bool visible = true;
    MAlphaNum defaultVal;
    MAlphaNum col = -1;
    MAlphaNum visibleValue;
    MString masterTableName;
    MString masterFieldName;
    MString caption;

};

struct MetaBool {
    Meta *meta;
    bool hasError;
};
inline Meta nullMeta = Meta("", Null, 0, 0, 0, 0, Normal, 0, 0, MAlphaNum(), -1);

Meta metaInt(const MString &name);

Meta *newMetaInt(const MString &name);

Meta metaStr(const MString &name);

Meta* newMetaStr(const MString &name);

Meta metaFloat(const MString &name);

Meta* newMetaFloat(const MString &name);

Meta metaInt64(const MString &name);

Meta* newMetaInt64(const MString &name);

MString $(Meta meta);

inline Meta id = metaInt64("id").setPrimary().setAutoInc().setInvisible().setColumn(0);
inline Meta idMaster = metaInt64("idMaster").setInvisible().setColumn(1);
const MString ID = "id";
const MString IDMaster = "idMaster";
const int IDIndex = 0;
const int IDMasterIndex = 1;

