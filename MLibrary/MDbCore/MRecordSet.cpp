//
// Created by MERHAB on 01/08/2023.
//

#include "MRecordSet.h"
#include "mdataset.h"

MRecordSet::MRecordSet(): _dataset(MDataSet(&records)) {
  
}

QVector<MRecord>& MRecordSet::getRecords() {
  return this->records;
}

 MDataSet<MRecord> &MRecordSet::dataset()  {
    return _dataset;
}
