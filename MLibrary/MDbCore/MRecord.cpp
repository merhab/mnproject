//
// Created by MERHAB on 01/08/2023.
//

#include "MRecord.h"
#include "MField.h"
#include "mdataset.h"
#include <cassert>

MRecord::MRecord() : fieldset(MDataSet(&fields)) {
  this->fields.append({MField(&id), MField(&idMaster)});
}

MRecord::MRecord(MetaList &metaList) : fieldset(MDataSet(&fields)) {
    for (size_t i = 0; i <metaList.getMetas() ; ++i) {

    }
  for (auto &meta : metaList.getMetas()) {
    this->append(MField(meta));
  }
}

MRecord::MRecord(MVector<Meta *> metas): fieldset(MDataSet(&fields)) {
    for (size_t i = 0; i <metas.count() ; ++i) {
        this->append(MField(metas[i]));
    }
}

MField& MRecord::fieldByName(MString name) {
    for (size_t i = 0; i < this->fields.count() ; ++i) {
        if (this->fields[i].getMeta()->getName() == name) {
            return this->fields[i];
        }
  }
  assert(false);
}

MField& MRecord::fieldByIndex(int index) {
  if (index < 0 or index >= this->fields.size()) {
    assert(false);
  }
  return (this->fields[index]);
}

MDataSet<MField> &MRecord::getFieldset() { return this->fieldset; }

void MRecord::setFieldset(const MDataSet<MField> &fieldset) {
  MRecord::fieldset = fieldset;
}

 MVector<MField> &MRecord::getFields()  { return fields; }

void MRecord::setFields(const MVector<MField> &fields) {
  MRecord::fields = fields;
}

int64_t MRecord::size() { return this->fields.size(); }

MField *MRecord::itemAt(int index) {
  if (index < 0 or index >= this->size())
    return nullptr;
  return &(this->fields[index]);
}

void MRecord::append(MField fld) {
  for(auto f:this->fields){
    if(f.getMeta()->getName()== fld.getMeta()->getName()){
      assert(false);
    }
  }
  this->fields.append(fld);
}

void MRecord::append(MVector<MField> flds) {
  for(auto f:flds){
    this->append(f);
  }
}

MStmt MRecord::remove(MString tableName) {
  assert(tableName != "");
  return {Delete,
          "DELETE FROM " + tableName + " WHERE id=?",
          {{this->fields[0].getMeta()->getName()}},
          {this->fields[0].getValue()}};
}

MStmt MRecord::insert(MString tableName, int64_t idMaster) {
  // we only insert new record that don`t have an Id
  assert(this->isNew());

  MStmt stmt;
  stmt.kind = Insert;
  MString str;
  MString names;
  this->getIDMasterField().setValue(idMaster);
  str += "?";
  names += IDMaster;
  stmt.values().append(idMaster);
  stmt.names().append(IDMaster);
  for (int i = 2; i < this->size(); ++i) {
    str = str + "," + "?";
    names = names + "," + this->itemAt(i)->getMeta()->getName();
    stmt.values().append({this->fields[i].getValue()});
    stmt.names().append({this->fields[i].getMeta()->getName()});
  }
  stmt.stmt =
      "INSERT INTO " + tableName + " (" + names + ")" + " VALUES (" + str + ")";
  return stmt;
}

MStmt MRecord::update(MString tableName) {
  MStmt stmt;
  stmt.kind = Update;
  stmt.stmt = this->getFirstField().getMeta()->getName() + "= ?";
  stmt.values().append({this->fields[this->firstIndex()].getValue()});
  stmt.names().append({this->fields[this->firstIndex()].getMeta()->getName()});
  for (int i = this->firstIndex()+1; i < this->size(); ++i) {
    stmt.stmt += "," + this->fields[i].getMeta()->getName() + "=?";
    stmt.values().append({this->fields[i].getValue()});
    stmt.names().append({this->fields[i].getMeta()->getName()});
  }

  stmt.stmt = "UPDATE " + tableName + " SET " + stmt.stmt + " WHERE id=?";
  stmt.values().append(this->getIDField().getValue());
  stmt.names().append(this->getIDField().getMeta()->getName());
  return stmt;
}

int64_t MRecord::getID() {
  assert(!this->fields.isEmpty());
  return this->fields[0].getValue().isValid()
             ? this->fields[0].getValue().toLongLong()
             : -1;
}

int64_t MRecord::getIDMaster() {
  assert(!this->fields.isEmpty());
  return this->fields[1].getValue().isValid()
             ? this->fields[1].getValue().toLongLong()
             : -1;
}

MField &MRecord::getIDField() {
  assert(!this->fields.isEmpty());
  return this->fields[0];
}

MField &MRecord::getIDMasterField() {
  assert(!this->fields.isEmpty());
  return this->fields[1];
}

MField &MRecord::getFirstField() {
  if (this->fields.size() > 2) {
    return this->fields[2];
  } else if (this->fields.size() == 2) {
    return this->fields[1];
  } else
    assert(false);
}

int MRecord::firstIndex() {
  if (this->fields.size() > 2) {
    return 2;
  } else if (this->fields.size() == 2) {
    return 1;
  } else
    assert(false);
}

bool MRecord::isDirty() const { return dirty; }

void MRecord::setDirty(bool dirty) { MRecord::dirty = dirty; }

bool MRecord::isDeleted() const { return deleted; }

void MRecord::setDeleted(bool deleted) { MRecord::deleted = deleted; }

bool MRecord::isNew() {
  return this->getID() == -1 or this->getIDField().getValue().isNull();
}

MString $d(MRecord record) {
  MString s;
  for (auto fld : record.getFields()) {
    s += $(fld);
  }
  return s;
}

MString $(MRecord record) {
    MString s="[";
    for (auto fld : record.getFields()) {
        s +=fld.getMeta()->getName() + ":"+fld.getValue().toString()+",";
    }
    return s+"]";
}

MString MRecord::names() {
  assert(!this->fields.isEmpty());
  MString b;
  b = this->fields[0].getMeta()->getName();
  for(int i =1;i<fields.size();i++){
    b+=","+fields[i].getMeta()->getName();
  }
  return b;
}

MStringList MRecord::namesList() {
  MStringList b;
  for(auto fld:fields){
    b.append(fld.getMeta()->getName());
  }
  return b;
}

void MRecord::setValues(MVector<MAlphaNum> vals) {
  //assert same number of vals or without id idMaster
  assert(vals.size()<= this->fields.size() || vals.size()>= this->fields.size()-2);
  int j =0;
  for(int i=this->fields.size()-vals.size();i<this->fields.size();i++){
    this->fields[i].setValue(vals[j]);
    j++;
  }
}


