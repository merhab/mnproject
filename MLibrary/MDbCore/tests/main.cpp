//
// Created by mac on 30/08/2023.
//
#include <iostream>
#include "meta_t.h"
int main(int args,char** arg){
    unsigned long start = clock();
    for (int i = 0; i <1 ; ++i){
        testMeta();
    }
    unsigned long end = clock();//Now check what amount of ticks we have now.
//To get the time, just subtract start from end, and divide by CLOCKS_PER_SEC.
    printGreen(arg[0]);
    printGreen("\n");
    std::cout << "it took " << end - start << "ticks, or " << ((float)end -(float) start)/CLOCKS_PER_SEC << "seconds." << std::endl;
    return 0;
}