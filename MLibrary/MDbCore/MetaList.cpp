//
// Created by mac on 03/08/2023.
//

#include "MetaList.h"

void MetaList::append(Meta* meta) {
    meta->setColumn(this->_metas.size());
    this->_metas.append(meta);
}

void MetaList::append(MVector<Meta*> metaList) {
    for (size_t i = 0; i <metaList.count() ; ++i) {
        metaList[i]->setColumn(this->_metas.size());
        this->append(metaList[i]);
    }
}

int64_t MetaList::size() {
    return this->_metas.size();
}

MetaBool MetaList::itemByName(MString name) {
    for (size_t i = 0; i < this->_metas.count(); ++i) {
        if (this->_metas[i]->getName() == name) {
            return {this->_metas[i], false};
        }
    }
    return {&nullMeta, true};
}

MetaBool MetaList::itemByIndex(int index) {
    if (index < 0 or index >= this->size())
        return {&nullMeta, true};
    return {this->_metas[index], false};
}

MetaList::MetaList(MString* tableName ) {
    this->_tableName = tableName;
    this->append({&id, &idMaster});
}

MStmt MetaList::createTable(MString tableName, MString engine) {
    MString kind = "";
    MStringList result;
    for (size_t i = 0; i < this->_metas.count() ; ++i) {
        MString str = "";
        switch (this->_metas[i]->getType()) {
            case Int:
            case Int64:
                kind = "INTEGER";
                break;
            case String:
                kind = "TEXT";
                break;
            case Float:
                kind = "REAL";
                break;
            default:
                assert(false);
                break;

            case Null:
                assert(false);
                break;
        }
        str = "`" + this->_metas[i]->getName() + "` " + kind;
        if (this->_metas[i]->isPrimary())
            str = str + " PRIMARY KEY";
        if (this->_metas[i]->isAutoInc()) {
            if (engine == "QSQLITE") {
                str = str + " AUTOINCREMENT";
            } else if (engine == "QMYSQL") {
                str = str + " AUTO_INCREMENT";
            } else
                assert(false);
        }
        if (this->_metas[i]->isUnic())
            str = str + " UNIQUE";
        if (this->_metas[i]->isMondatory())
            str = str + " NOT NULL";
        if (this->_metas[i]->getDefaultVal().isValid()) {
            str = str + " DEFAULT ";
            if (this->_metas[i]->getType() == String)
                str = str + "\"" + this->_metas[i]->getDefaultVal().toString() + "\"";
            else
                str = str + this->_metas[i]->getDefaultVal().toString();
        }
        result.append(str);
    }


    MStmt stmt;
    stmt.kind = CreateTable;

    stmt.stmt =
            "CREATE TABLE IF NOT EXISTS `" + tableName + "`(" +
            result.join(",") + ")";
    return stmt;
}

bool MetaList::isEmpty() {
  return this->_metas.isEmpty();
}

const MVector<Meta *> &MetaList::getMetas() const {
    return _metas;
}

void MetaList::setMetas(const MVector<Meta *> &metas) {
    _metas = metas;
}

MString *MetaList::getTableName(){
    return _tableName;
}

void MetaList::setTableName( MString *tableName) {
    _tableName = tableName;
}
