//
// Created by MERHAB on 01/08/2023.
//

#pragma once
#include "MRecord.h"
#include "MStmt.h"
#include <sys/_types/_int64_t.h>

enum CmdType { SelectCmd, UpdateCmd, InsertCmd, DeleteCmd, CreateTableCmd };
class MStmtList {
protected:
  QVector<MStmt> list;
  CmdType cmdType = SelectCmd;
  MStmt preparedStmt;
  int64_t idMaster = -1;
  int64_t limit = -1;
  int64_t offset = -1;

public:
  const QVector<MStmt> &getList() const;

  void setList(const QVector<MStmt> &list);

  CmdType getCmdType() const;

  MStmtList &setCmdType(CmdType cmdType);

  MStmt &getPreparedStmt() ;

  void setPreparedStmt(const MStmt &preparedStmt);

  int64_t getIdMaster() const;

  MStmtList &setIdMaster(int64_t idMaster);

  int64_t getLimit() const;

  void setLimit(int64_t limit);

  int64_t getOffset() const;

  void setOffset(int64_t offset);
  MStmtList();
  MStmtList(const MStmtList &list);
  MStmtList &prepare();
  MStmtBool locate(MStmtKind kind);
  MStmtList &append(MStmt stmt);
  MStmtBool itemAt(int index);
  int size();
  MStmt *getLast();
  bool isEmpty();
};
QString $(MStmtList list);
inline MStmtList nullMStmtList = MStmtList();
struct MStmtListBool {
  MStmtList *stmts;
  bool hasError;
  QString error;
};
