//
// Created by MERHAB on 31/07/2023.
//

#pragma once
#include "MAlphaNumList.h"
#include "MetaList.h"

class MField{
protected:
    Meta* meta;
    MAlphaNum value;
public:
  MField(Meta* meta,MAlphaNum val);
  MField(const MField& fld);
  MField(Meta* meta);
  Meta* getMeta();
  MAlphaNum getValue();
  void setMeta(Meta* meta);
  void setValue(MAlphaNum value);
};
MString $(MField fld);
MField *newMField(Meta* meta,MAlphaNum val);
