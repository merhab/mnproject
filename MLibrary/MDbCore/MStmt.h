//
// Created by MERHAB on 01/08/2023.
//
#pragma once
#include "../Core/MString.h"
#include "../Core/MAlphaNum.h"
#include "../Core/MStringList.h"
#include "../Core/MAlphaNumList.h"
struct NamedVal{
  MString name;
  MAlphaNum val;
};
enum MStmtKind{AndFilter,OrFilter,Select,Update,OrderBy,CreateTable,Insert,Delete};
inline MString $(MStmtKind kind){
    switch (kind) {

        case AndFilter:
            return "AndFilter";
            break;
        case OrFilter:
            return "OrFilter";
            break;
        case Select:
            return "Select";
            break;
        case Update:
            return "Update";
            break;
        case OrderBy:
            return "OrderBy";
            break;
        case CreateTable:
            return "CreateTable";
            break;
        case Insert:
            return "Insert";
            break;
        case Delete:
            return "Delete";
            break;
        default:
            return "ERROR";
            break;
    }
}
class MStmt {
protected:
    MStringList _names;
    MAlphaNumList _values;
public:
    void setValues(const MAlphaNumList &values);
    void setNames(const MStringList &names);
    MStmt();
    MStmt(const MStmt &stmt);
    MStmt(MStmtKind kind, MString stmt ,MStringList names ,MAlphaNumList values ={});
    MStringList &names();
    MAlphaNumList &values();
    //QVector<NamedVal> vals;
    MString stmt;
    MStmtKind kind;
};
inline auto nullMStmt = MStmt();
struct MStmtBool{
    MStmt* mStmt;
    bool hasError;
};
MString $(MStmt self);

MStmt operator&&(MStmt s1,  MStmt &s2);

MStmt operator||(MStmt s1,  MStmt &s2);

MStmt operator!(MStmt s1);

MStmt operator==(MStmt s, const MAlphaNum &val);

MStmt operator>(MStmt s, const MAlphaNum &val);

MStmt operator>=(MStmt s, const MAlphaNum &val);

MStmt operator<(MStmt s, const MAlphaNum &val);

MStmt operator<=(MStmt s, const MAlphaNum &val);

MStmt operator%(MStmt s, const MAlphaNum &val);

MStmt filter(MStringList names, MAlphaNumList vals = {});
MStmt filter(MString name);





