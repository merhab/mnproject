//
// Created by MERHAB on 31/07/2023.
//
#include "MField.h"
#include <cassert>

MField::MField(Meta* meta, MAlphaNum val) {
  this->meta = meta;
  this->setValue(val);
}

MField::MField(Meta *meta) {
    this->meta = meta;
    this->value = meta->initVal();
}

MField::MField(const MField& fld) {
  this->meta = fld.meta;
  this->value = fld.value;
}

Meta* MField::getMeta() {
  return this->meta;
}

MAlphaNum MField::getValue() {
  return this->value;
}

void MField::setMeta(Meta* meta) {
  this->meta = meta;
}

void MField::setValue(MAlphaNum value) {
    //qDebug()<<this->meta->getQtType() << " == " << QString(value().name()) <<"\n";
    //assert(this->meta->getQtType()== QString(value.metaType().name()));
    if (this->meta->getType() == Int or this->meta->getType() == Int64){
        assert(value.isNumber() && !value.getNumber().isDecimal());
    } else if(this->meta->getType() == String) assert(value.isMstring());
    else if(this->meta->getType()==Float) assert(value.isNumber() && value.getNumber().isDecimal());
    else if(this->meta->getType()==Null) assert(value.isNil());
    else
        assert(false);
  this->value = value;
}


MString $(MField fld){
    return "{meta:"+ $(*fld.getMeta())+",val:"+fld.getValue().toString()+"}\n";
}

MField *newMField(Meta *meta, MAlphaNum val) {
    return new MField(meta,val);
}
