//
// Created by MERHAB on 03/08/2023.
//
#pragma once

#include "meta.h"
#include "MStmt.h"

class MetaList {
public:
protected:
    MVector<Meta *> _metas;
    MString *_tableName;
public:
    const MVector<Meta *> &getMetas() const;

    void setMetas(const MVector<Meta *> &metas);

    MString *getTableName();

    void setTableName(MString *tableName);

    void append(Meta *meta);

    void append(MVector<Meta *> metaList);

    int64_t size();

    MetaBool itemByName(MString name);

    MetaBool itemByIndex(int index);

    MStmt createTable(MString tableName, MString engine = "QSQLITE");

    bool isEmpty();

    MetaList(MString* tableName = nullptr);


};



