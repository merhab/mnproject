#pragma once

#include "MVector.h"

template <typename T>
class MDataSet{
protected:
    int64_t index=-1;
    MVector<T> *data;
    MVector<bool (*)(MDataSet<T>* dataSet,int64_t indexCurrent,int64_t indexAfter)> canMoveFuncs;
    MVector<void (*)(MDataSet<T>* dataSet,int64_t indexCurrent,int64_t indexAfter)> beforeMoveFuncs;
    MVector<void (*)(MDataSet<T>* dataSet,int64_t indexAfter)> afterMoveFuncs;
    bool checkCanMove(int64_t indexAfter);
    void doBeforeMove(int64_t indexAfter);
    void doAfterMove(int64_t indexAfter);
    MVector<bool (*)(MDataSet<T>* dataSet,int64_t indexCurrent,int64_t indexAfter)> canRemoveFuncs;
    MVector<void (*)(MDataSet<T>* dataSet,int64_t indexCurrent,int64_t indexAfter)> beforeRemoveFuncs;
    MVector<void (*)(MDataSet<T>* dataSet,int64_t indexAfter)> afterRemoveFuncs;
    bool checkCanRemove(int64_t indexAfter){
        for(auto fn: this->canRemoveFuncs){
            if(!fn(this,index,indexAfter)){
                return false;
            }
        }
        return true;
    }
    void doBeforeRemove(int64_t indexAfter){
        for(auto fn: this->beforeRemoveFuncs)
            fn(this,index,indexAfter);
    }
    void doAfterRemove(int64_t indexAfter){
        for(auto fn: this->afterRemoveFuncs)
            fn(this,indexAfter);
    }

    MVector<bool (*)(MDataSet<T>* dataSet,int64_t indexCurrent,int64_t indexAfter)> canAppendFuncs;
    MVector<void (*)(MDataSet<T>* dataSet,int64_t indexCurrent,int64_t indexAfter)> beforeAppendFuncs;
    MVector<void (*)(MDataSet<T>* dataSet,int64_t indexAfter)> afterAppendFuncs;
    bool checkCanAppend(int64_t indexAfter){
        for(auto fn: this->canAppendFuncs){
            if(!fn(this,index,indexAfter)){
                return false;
            }
        }
        return true;
    }
    void doBeforeAppend(int64_t indexAfter){
        for(auto fn: this->beforeAppendFuncs)
            fn(this,index,indexAfter);
    }
    void doAfterAppend(int64_t indexAfter){
        for(auto fn: this->afterAppendFuncs)
            fn(this,indexAfter);
    }

public:
    explicit MDataSet(MVector<T> *dataset);
    T &current();
  int64_t currentIndex(){
    return index;
  }
    bool eof();
    bool bof();
    bool goTo(int64_t index);
    bool first();
    bool next();
    bool prior();
    bool last();
  bool remove(){
      assert(index >-1);
      int64_t ind;
      if(data->size()==1){
          ind = -1;
      }else if(index == 0){
          ind =0;
      } else{
          ind = index -1;
      }
      if(!checkCanRemove(ind))return false;
      if(!checkCanMove(ind)) return false;
      doBeforeRemove(ind);
      doBeforeMove(ind);
    this->data->removeAt(index);
      index = ind;
      doAfterRemove(ind);
      this->doAfterMove(ind);
      return true;
  }

    int64_t size(){
        return this->data->size();
  }

  bool append(T item){
      int64_t ind;
      ind = data->size();
      if(!checkCanAppend(ind))return false;
      if(!checkCanMove(ind)) return false;
      doBeforeAppend(ind);
      doBeforeMove(ind);
      data->append(item);
      index = ind;
      doAfterAppend(ind);
      this->doAfterMove(ind);
      return true;
  }

void clear(){
    this->data->clear();
    index = -1;
  };
    void append_beforeMoveFuncs(void (*fn)(MDataSet<T>* dataSet,int64_t indexCurrent,int64_t indexAfter)) {
        this->beforeMoveFuncs.append(fn);
    }
    void append_canMoveFuncs(bool (*fn)(MDataSet<T>* dataSet,int64_t indexCurrent,int64_t indexAfter)) {
        this->canMoveFuncs.append(fn);
    }
    void append_afterMoveFuncs(void (*fn)(MDataSet<T>* dataSet,int64_t indexAfter)) {
        this->afterMoveFuncs.append(fn);
    }
    void append_beforeRemoveFuncs(void (*fn)(MDataSet<T>* dataSet,int64_t indexCurrent,int64_t indexAfter)) {
        this->beforeRemoveFuncs.append(fn);
    }
    void append_canRemoveFuncs(bool (*fn)(MDataSet<T>* dataSet,int64_t indexCurrent,int64_t indexAfter)) {
        this->canRemoveFuncs.append(fn);
    }
    void append_afterRemoveFuncs(void (*fn)(MDataSet<T>* dataSet,int64_t indexAfter)) {
        this->afterRemoveFuncs.append(fn);
    }
    void append_beforeAppendFuncs(void (*fn)(MDataSet<T>* dataSet,int64_t indexCurrent,int64_t indexAfter)) {
        this->beforeAppendFuncs.append(fn);
    }
    void append_canAppendFuncs(bool (*fn)(MDataSet<T>* dataSet,int64_t indexCurrent,int64_t indexAfter)) {
        this->canAppendFuncs.append(fn);
    }
    void append_afterAppendFuncs(void (*fn)(MDataSet<T>* dataSet,int64_t indexAfter)) {
        this->afterAppendFuncs.append(fn);
    }
};
template<typename T>
MDataSet<T>::MDataSet(MVector<T> *data) {
    this->data = data;
    if (!data->isEmpty()) index =0;
}

template <typename T>bool MDataSet<T>:: checkCanMove(int64_t indexAfter) {

    for(auto fn:this->canMoveFuncs){
        if(!fn(this,index,indexAfter)) return false;
    }
    return true;
}

template <typename T>void MDataSet<T>:: doBeforeMove(int64_t indexAfter) {
    for(auto fn:this->beforeMoveFuncs){
        fn(this,index,indexAfter);
    }
}

template <typename T>void MDataSet<T>:: doAfterMove(int64_t indexAfter) {
    for(auto fn:this->afterMoveFuncs){
        fn(this,indexAfter);
    }
}

template <typename T> bool MDataSet<T>::eof()  {
    return index == this->data->size()-1;
}

template <typename T> bool MDataSet<T>::bof()  {
    return index == 0;
}

template<typename T>
T& MDataSet<T>::current() {
    assert(!data->isEmpty());
    if (index == -1) index =0;
    auto& l = *(this->data);
    return l[index];
}

template<typename T>
bool MDataSet<T>::goTo(int64_t index) {
    assert(!data->empty());
    if(this->index == -1) this->index = 0;
    if(index <0 or index >= this->data->size()){
        return false;
    }
    if(!checkCanMove(index)) return false;
    doBeforeMove(index);
    this->index = index;
    doAfterMove(index);
    return true;
}

template<typename T>
bool MDataSet<T>::first() {
    return goTo(0);
}

template<typename T>
bool MDataSet<T>::next() {
    return goTo(index+1);
}

template<typename T>
bool MDataSet<T>::prior() {
    return goTo(index-1);
}

template<typename T>
bool MDataSet<T>::last() {
    return goTo(this->data->size()-1);
}
