//
// Created by mac on 26/08/2023.
//

#include "MDbAbleGui.h"

MField *MDbAbleGui::field() const {
    return _field;
}

void MDbAbleGui::setField(MField *field) {
    _field = field;
}

int MDbAbleGui::getCol() const {
    return _col;
}

void MDbAbleGui::setCol(int col) {
    _col = col;
}

const QString &MDbAbleGui::getFieldName() const {
    return _fieldName;
}

void MDbAbleGui::setFieldName(const QString &fieldName) {
    _fieldName = fieldName;
}

MDbAbleGui::MDbAbleGui(MField *fld) {
        _field = fld;

}
