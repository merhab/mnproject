#pragma once
#include <stdio.h>
/* #include <QTextStream> */
/* #include <QString> */
/* #include <QVariant> */

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"
inline QTextStream out(stdout);
enum Colors{Red,Yellow,Green,Blue,Magenta,Cyan};
inline QString coloredText(QVariant var, Colors color)
{
    QString result="";
    QString clr="";
    switch (color) {
        case Red:
            clr = QString(ANSI_COLOR_RED);
            break;
        case Yellow:
            clr = QString(ANSI_COLOR_YELLOW);
            break;
        case Green:
            clr = QString(ANSI_COLOR_GREEN);
            break;
        case Blue:
            clr = QString(ANSI_COLOR_BLUE);
            break;
        case Magenta:
            clr = QString(ANSI_COLOR_MAGENTA);
            break;
        case Cyan:
            clr = QString(ANSI_COLOR_CYAN);
            break;
    }
    return clr+var.toString()+QString(ANSI_COLOR_RESET);
}
inline QString red(QVariant var){
    return coloredText(var,Red);
}
inline QString yellow(QVariant var){
    return coloredText(var,Yellow);
}
inline QString green(QVariant var){
    return coloredText(var,Green);
}
inline QString blue(QVariant var){
    return coloredText(var,Blue);
}
inline QString magenta(QVariant var){
    return coloredText(var,Magenta);
}
inline QString cyan(QVariant var){
    return coloredText(var,Cyan);
}
inline bool test(bool willPass, QString message)
{
    if (willPass) {
        out << blue(message);
        out << green("----> passed");
        out << green("\n");

    }
    else {
        out << blue(message);
        out << red("----> did not pass");
        out << red("\n");

    }
    return willPass;
}
inline bool test_v1(bool bool_res)
{
    return test(bool_res, "working\n");
}

