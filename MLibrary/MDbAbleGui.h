//
// Created by MERHAB on 26/08/2023.
//
#pragma once
#include "MField.h"
#include "MEventAble.h"
#include "test.h"
struct MValidationResult{
    bool isValid = true;
    QString message="";
};
class MDbAbleGui: public MEventAble {
protected:
    MField* _field=0;
    int _col=-1;
    QString _fieldName;
public:
    enum {MdbAbleGuiValidate};
    explicit MDbAbleGui(MField* fld);
    const QString &getFieldName() const;

    void setFieldName(const QString &fieldName);

    int getCol() const;

    void setCol(int col);

    MField *field() const;

    void setField(MField *field);

    virtual void setValue(QVariant variant) =0;

    virtual QVariant value() =0;

    virtual MValidationResult isValid() = 0;

    //void appendAfterValidateEvent(MEvent event);
};



