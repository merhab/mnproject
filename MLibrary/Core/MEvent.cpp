#include "MEvent.h"

 MString &MEvent::name()  {
    return _name;
}

void MEvent::setName(const MString &name) {
    _name = name;
}

 int MEvent::type()  {
    return _type;
}

void MEvent::setType(int type) {
    _type = type;
}

void MEvent::setFunc(MEventFunc func) {
    _func = func;
}

void MEvent::execFunc(void* sender,void* receiver,MAlphaNumList args) {
  _func(sender,receiver,args);
}

bool MEvent::execCanFunc(void* sender,void* receiver,MAlphaNumList args) {
  auto ret= _func(sender,receiver,args);
  //todo: remove assertion to gain speed
    assert(!ret.isEmpty());
    assert(ret[0].isNumber() && ret[0].getNumberType() == MBool);
    return ret[0].getNumber().tooBool();
}

static int mEventNamingInc=0;
MEvent::MEvent(MEventFunc function, int type, MString name) {
    if(name ==""){
        mEventNamingInc++;
        name = "MEvent" + MString::fromInt(mEventNamingInc);
    }
    this->_name = name;
    this->_type = type;
    this->_func = function;
}
