//
// Created by MERHAB on 31/08/2023.
//

#pragma once
#include "MAlphaNum.h"

const static char* IntName = "MInt";
const static char* LongName = "MLong";
const static char* LongLongName = "MLongLong";
const static char* FloatName = "MFloat";
const static char* DoubleName = "MDouble";
const static char* LongDoubleName = "MLongDouble";
const static char* BoolName = "MBool";
const static char* PointerName = "MPointer";
const static char* NilName = "MNil";
const static char* MStringName = "MString";
const static char *MUCharName = "MUCharName";
const static char *MUShortIntName="MUShortIntName";
const static char *MUIntName="MUIntName";
static const char *MULongIntName="MULongIntName";
const static char *MULongLongIntName="MULongLongIntName";
static const char *MCharName="MCharName";
static const char *MLongCharName="MLongCharName";
static const char *MShortIntName="MShortIntName";
class MVariant {
protected:
    bool _isAlphaNum;
       MAlphaNum _alphaNum;
       MSharedData _data;
    const char* _typeName;
public:
  const char* getTypeName();//tested
  MVariant(const MVariant& variant);//tested
  MVariant &operator=(const MVariant &variant);//tested
    //MVariant &operator=(const MNumber &variant);
  MVariant(MAlphaNum alphaNum);
     MVariant(const bool b):MVariant(MNumber(b)){}
    MVariant(const char *string): MVariant(MAlphaNum(MString(string))){}
    //MVariant(MNumber number);
    MVariant(unsigned char uc):MVariant(MNumber(uc)){}

    MVariant(unsigned short int usi):MVariant(MNumber(usi)){}

    MVariant(unsigned int ui):MVariant(MNumber(ui)){}

    MVariant(unsigned long uli):MVariant(MNumber(uli)){}

    MVariant(unsigned long long ulli):MVariant(MNumber(ulli)){}

    MVariant(char c):MVariant(MNumber(c)){}

    MVariant(wchar_t lc):MVariant(MNumber(lc)){}

    MVariant(short int si):MVariant(MNumber(si)){}

    MVariant(int i):MVariant(MNumber(i)){}

    MVariant(long li):MVariant(MNumber(li)){}

    MVariant(long long lli):MVariant(MNumber(lli)){}

    MVariant(float f):MVariant(MNumber(f)){}

    MVariant(double d):MVariant(MNumber(d)){}

    MVariant(long double ld):MVariant(MNumber(ld)){}



    MVariant(void* p):MVariant(MNumber(p)){}

    MVariant(MString const string):MVariant(MAlphaNum(string)){};
    MVariant(MString &string){
        this->_isAlphaNum = true;
        this->_alphaNum = MAlphaNum(string);
    }
    MVariant(const char* &cstring);
  MVariant();//tested
  MVariant(MSharedData &variantData);
  ~MVariant(){};
  int toInt();//tested
  long toLong();//tested
  long long toLongLong();//tested
  float toFloat();//tested
  double toDouble();//tested
  long double toLongDouble();//tested
  bool toBool();//tested
  void* toPointer();//tested
  bool isNil();//tested
  MString toMString(char decimal=0);//tested
};

