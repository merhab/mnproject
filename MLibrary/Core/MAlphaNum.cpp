//
// Created by MERHAB NOUREDDINE on 05/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#include "MAlphaNum.h"
#include <cassert>
#include <climits>

MAlphaNum::MAlphaNum(MNumber number) {
    this->_isNumber = true;
    this->_number =number;
}

MAlphaNum::MAlphaNum(MString string) {
    _string = string;
    _isNumber  = false;
}

MAlphaNum::MAlphaNum(const char *cstring) {
    _string = MString(cstring);
    _isNumber = false;
}

MAlphaNum::MAlphaNum(const MAlphaNum &alphaNum) {
    this->_isNumber = alphaNum._isNumber;
    if (alphaNum._isNumber)
        this->_number = alphaNum._number;
    else
        this->_string = alphaNum._string;
}

MAlphaNum &MAlphaNum::operator=(const MAlphaNum &alphaNum) {
    if (this == &alphaNum)
        return *this;
    this->_isNumber = alphaNum._isNumber;
    if (alphaNum._isNumber)
        this->_number = alphaNum._number;
    else
        this->_string = alphaNum._string;
    return *this;
}


bool MAlphaNum::isNumber() const {
    return _isNumber;
}

bool MAlphaNum::isMstring() {
    return !_isNumber;
}

bool MAlphaNum::isText() {
    return !_isNumber;
}

MAlphaNum MAlphaNum::operator+(MAlphaNum alphaNum) {
    if(this->isNumber() && alphaNum.isNumber()){
        return this->_number + alphaNum._number;
    } else if(this->isMstring() && alphaNum.isMstring()){
        return this->_string + alphaNum._string;
    } else{
        MNumber* n;
        MString* s;
        if(this->_isNumber){
            n = &(this->_number);
            s = &(alphaNum._string);
        } else{
            s = &(this->_string);
            n = &(alphaNum._number);
        }
        char str[500];
        n->toCString(str,500,0);
        return *s + str;
    }
}

MAlphaNum MAlphaNum::operator-(MAlphaNum alphaNum) {
    if(this->isNumber() && alphaNum.isNumber())
        return this->_number - alphaNum._number;
    else {
        printf("%s", "cant perform - operator on MString and Number\n"
                     "convert the MString to a number first\n");
        assert(false);
    }
}

MAlphaNum MAlphaNum::operator*(MAlphaNum alphaNum) {
    if(this->isNumber() && alphaNum.isNumber())
        return this->_number * alphaNum._number;
    else {
        printf("%s", "cant perform * operator on MString and Number\n"
                     "convert the MString to a number first\n");
        assert(false);
        abort();
    }
    return 0;
}

MAlphaNum MAlphaNum::operator/(MAlphaNum alphaNum) {
    if(this->isNumber() && alphaNum.isNumber())
        return this->_number / alphaNum._number;
    else {
        printf("%s", "cant perform / operator on MString and Number\n"
                     "convert the MString to a number first\n");
        assert(false);
    }
}

MAlphaNum MAlphaNum::operator%(MAlphaNum alphaNum) {
    if(this->isNumber() && alphaNum.isNumber())
        return this->_number % alphaNum._number;
    else {
        printf("%s", "cant perform % operator on MString and Number\n"
                     "convert the MString to a number first\n");
        assert(false);
    }
}

MAlphaNum &MAlphaNum::operator++() {
    if(this->isNumber())
        this->_number++;
    else {
        printf("%s", "cant perform - operator on MString and Number\n"
                     "convert the MString to a number first\n");
        assert(false);
    }
    return *this;
}

MAlphaNum MAlphaNum::operator++(int) {
    MAlphaNum old = *this; // copy old value
    operator++();  // prefix increment
    return old;
}

MAlphaNum &MAlphaNum::operator--() {
    if(this->isNumber())
        this->_number--;
    else {
        printf("%s", "cant perform - operator on MString and Number\n"
                     "convert the MString to a number first\n");
        assert(false);
    }
    return *this;
}

MAlphaNum MAlphaNum::operator--(int) {
    MAlphaNum old = *this; // copy old value
    operator--();  // prefix increment
    return old;
}

bool MAlphaNum::operator==(MAlphaNum alphaNum) {
    if(this->isNumber() && alphaNum.isNumber()){
        return this->_number == alphaNum._number;
    } else if(this->isMstring() && alphaNum.isMstring()){
        return this->_string == alphaNum._string;
    } else{
        printf("%s", "cant perform == operator on MString and Number\n"
                     "convert the MString to a number first\n");
        assert(false);
    }
}

bool MAlphaNum::operator>(MAlphaNum alphaNum) {
    if(this->isNumber() && alphaNum.isNumber()){
        return this->_number > alphaNum._number;
    } else if(this->isMstring() && alphaNum.isMstring()){
        return this->_string > alphaNum._string;
    } else{
        printf("%s", "cant perform > operator on MString and Number\n"
                     "convert the MString to a number first\n");
        assert(false);
    }
}

bool MAlphaNum::operator<(MAlphaNum alphaNum) {
    if(this->isNumber() && alphaNum.isNumber()){
        return this->_number < alphaNum._number;
    } else if(this->isMstring() && alphaNum.isMstring()){
        return this->_string < alphaNum._string;
    } else{
        printf("%s", "cant perform > operator on MString and Number\n"
                     "convert the MString to a number first\n");
        assert(false);
    }
}

unsigned char MAlphaNum::toUnsignedChar() {
    MNumber number;
    if(isNumber())
        number =  this->_number;
    else{
        number =  MNumber(this->_string.toMaxUint());
    }
    return number.toUnsignedChar();
}

unsigned short int MAlphaNum::toUnsignedShortInt() {
    MNumber number;
    if(isNumber())
        number =  this->_number;
    else{
        number =  MNumber(this->_string.toMaxUint());
    }
    return number.toUnsignedShortInt();
}

unsigned int MAlphaNum::toUnsignedInt() {
    MNumber number;
    if(isNumber())
        number =  this->_number;
    else{
        number =  MNumber(this->_string.toMaxUint());
    }
    return number.toUnsignedInt();
}

unsigned long int MAlphaNum::toUnsignedLongInt() {
    MNumber number;
    if(isNumber())
        number =  this->_number;
    else{
        number =  MNumber(this->_string.toMaxUint());
    }
    return number.toUnsignedLongInt();
}

unsigned long long int MAlphaNum::toUnsignedLongLongInt() {
    MNumber number;
    if(isNumber())
        number =  this->_number;
    else{
        number =  MNumber(this->_string.toMaxUint());
    }
    return number.toUnsignedLongLongInt();
}

char MAlphaNum::toChar() {
    MNumber number;
    if(isNumber())
        number =  this->_number;
    else{
        number =  MNumber(this->_string.toMaxInt());
    }
    return number.toChar();
}

wchar_t MAlphaNum::toLongChar() {
    MNumber number;
    if(isNumber())
        number =  this->_number;
    else{
        number =  MNumber(this->_string.toMaxInt());
    }
    return number.toLongChar();
}

short int MAlphaNum::toShortInt() {
    MNumber number;
    if(isNumber())
        number =  this->_number;
    else{
        number =  MNumber(this->_string.toMaxInt());
    }
    return number.toShortInt();
}

int MAlphaNum::toInt() {
    MNumber number;
    if(isNumber())
        number =  this->_number;
    else{
        number =  MNumber(this->_string.toMaxInt());
    }
    return number.toInt();
}

long int MAlphaNum::toLongInt() {
    MNumber number;
    if(isNumber())
        number =  this->_number;
    else{
        number =  MNumber(this->_string.toMaxInt());
    }
    return number.toLongInt();
}

long long int MAlphaNum::toLongLongInt() {
    MNumber number;
    if(isNumber())
        number =  this->_number;
    else{
        number =  MNumber(this->_string.toMaxInt());
    }
    return number.toLongLongInt();
}

float MAlphaNum::toFloat() {
    MNumber number;
    if(isNumber())
        number =  this->_number;
    else{
        number =  MNumber(this->_string.toMaxDouble());
    }
    return number.toFloat();
}

double MAlphaNum::toDouble() {
    MNumber number;
    if(isNumber())
        number =  this->_number;
    else{
        number =  MNumber(this->_string.toMaxDouble());
    }
    return number.toDouble();
}

long double MAlphaNum::toLongDouble() {
    MNumber number;
    if(isNumber())
        number =  this->_number;
    else{
        number =  MNumber(this->_string.toMaxDouble());
    }
    return number.toLongDouble();
}

bool MAlphaNum::tooBool() {
    MNumber number;
    if(isNumber())
        number =  this->_number;
    else{
        number =  MNumber(this->_string.toBool());
    }
    return number.tooBool();
}

void *MAlphaNum::toPointer() {
    if (isNumber())
        return this->_number.getPointer();
    else{
        printf("%s\n","this is not a pointer");
        assert(false);
    }
}

bool MAlphaNum::isNil() {
    if (isNumber())
        return this->_number.isNil();
    else{
        return false;
    }
}

void MAlphaNum::toCString(char *strPtr, size_t size, int decimal) {
    if (isNumber())
         this->_number.toCString(strPtr,size,decimal);
    else{
        copy(strPtr, this->_string.getCString());
    }
}

bool MAlphaNum::operator<=(MAlphaNum alphaNum) {
    return *this < alphaNum || *this == alphaNum;
}

bool MAlphaNum::operator>=(MAlphaNum alphaNum) {
    return *this > alphaNum || *this == alphaNum;
}

MAlphaNum::MAlphaNum(): MAlphaNum(MNumber()) {

}

MNumber &MAlphaNum::getNumber() {
    return this->_number;
}

const MString MAlphaNum::getMString() const {
    return _string;
}

MString MAlphaNum::toString(int decimal)  {
    if (this->isMstring()){
        return this->_string;
    } else{
        char str[500];
        this->_number.toCString(str,500,decimal);
        return {str};
    }
}

bool MAlphaNum::isValid() {
    return !isNil();
}












