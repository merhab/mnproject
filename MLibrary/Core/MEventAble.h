//
// Created by MERHAB on 27/08/2023.
//
#pragma once
#include "MEventList.h"

class MEventAble {
protected:
    MVector<MEventList> events;
public:
    void appendEventList(MEventList eventList);
    void removeEventList(MString name);
    bool runCanEvents(int type,MAlphaNumList args={});
    void runEvents(int type,MAlphaNumList args={});
};
 
