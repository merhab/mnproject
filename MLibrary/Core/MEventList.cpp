//
// Created by MERHAB on 19/08/2023.
//

#include "MEventList.h"
#include <cassert>

static int mEventListNamingInc=0;
MEventList::MEventList(void* sender,void* receiver,MString name) {
  this->_receiver = receiver;
    this->_sender = sender;
  if(name==""){
      mEventListNamingInc++;
      name = "MEventList"+MString::fromInt(mEventListNamingInc);
  }
  this->_name = name;
}

MString MEventList::name() {
  return _name;
}

MEventList& MEventList::registerEvent(MEvent event) {
    for(size_t i=0;i<this->eventList.count();i++){
        if(this->eventList[i].name()==event.name())
            assert(false);//cant have same name
    }
    this->eventList.append(event);
    return *this;
}

void MEventList::unregisterEvent(MString name) {
  for(size_t i=0;i<this->eventList.count();i++){
    if(this->eventList[i].name()==name){
      this->eventList.removeAt(i);
    }
  }
}

void MEventList::runEvent(int eventType,MAlphaNumList args) {
    if(!_isConnect) return;
    for (size_t i = 0; i < this->eventList.count() ; ++i) {
        if(this->eventList[i].type()==eventType){
            this->eventList[i].execFunc(_sender,_receiver,args);
        }
    }
}


bool MEventList::runCanEvent(int eventType,MAlphaNumList args) {
  if(!_isConnect) return true;
    for (size_t i = 0; i < this->eventList.count() ; ++i) {
        if(this->eventList[i].type()==eventType){
            if(!this->eventList[i].execCanFunc(_sender,_receiver,args)){
                return false;
      }
    }
  }
  return true;
}

void MEventList::connect() {
  this->_isConnect = true;
}

void MEventList::disconnect() {
  this->_isConnect = false;
}

void MEventList::setReceiver(void *receiver) {
    _receiver = receiver;
}

void MEventList::setSender(void *sender) {
    _sender = sender;
}

bool MEventList::isGuiConnected() {
    return _isConnect;
}

void MEventList::setName(const MString &name) {
    _name = name;
}




