//
// Created by MERHAB NOUREDDINE on 15/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#pragma once
#include "MAlphaNum.h"
#include "MVector.h"

class MAlphaNumList {
    MVector<MAlphaNum> _list;
public:
    MAlphaNum &operator[](size_t idx) ;

    const MAlphaNum &operator[](size_t idx) const ;

    size_t count();

    size_t size();

    MAlphaNumList& append(MAlphaNum val);
    MAlphaNumList(std::initializer_list<MAlphaNum> items);
    MAlphaNumList& append(std::initializer_list<MAlphaNum> items);
    bool isEmpty();
};
