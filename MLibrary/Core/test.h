//
// Created by MERHAB on 30/08/2023.
//

#pragma once

#include <cstdio>
#include "MString.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"
enum Colors{Red,Yellow,Green,Blue,Magenta,Cyan};
inline void print(const char* var, Colors color)
{
    switch (color) {
        case Red:
        printf("%s%s%s\n",ANSI_COLOR_RED,var,ANSI_COLOR_RESET);
            break;
        case Yellow:
            printf("%s%s%s\n",ANSI_COLOR_YELLOW,var,ANSI_COLOR_RESET);
            break;
        case Green:
            printf("%s%s%s\n",ANSI_COLOR_GREEN,var,ANSI_COLOR_RESET);
            break;
        case Blue:
            printf("%s%s%s\n",ANSI_COLOR_BLUE,var,ANSI_COLOR_RESET);
            break;
        case Magenta:
            printf("%s%s%s\n",ANSI_COLOR_MAGENTA,var,ANSI_COLOR_RESET);
            break;
        case Cyan:
            printf("%s%s%s\n",ANSI_COLOR_CYAN,var,ANSI_COLOR_RESET);
            break;
    }
    
}

inline void printRed(const char* var){
     print(var,Red);
}

inline void printRed(MString var){
    print(var.cstring(),Red);
}
inline void printYellow(const char* var){
    return print(var,Yellow);
}
inline void printYellow(MString var){
    return print(var.cstring(),Yellow);
}
inline void printGreen(const char* var){
    return print(var,Green);
}
inline void printGreen(MString &var){
    return print(var.cstring(),Green);
}
inline void printBlue(const char* var){
    return print(var,Blue);
}
inline void printBlue(MString &var){
    return print(var.cstring(),Blue);
}
inline void printMag(const char* var){
    return print(var,Magenta);
}
//inline void printMag(MString &var){
//    return print(var.cstring(),Magenta);
//}
inline void printMag(MString var){
    return print(var.cstring(),Magenta);
}
inline void printCyan(const char* var){
    return print(var,Cyan);
}
inline void printCyan(MString &var){
    return print(var.cstring(),Cyan);
}

inline bool test(bool willPass, const char* message)
{
    if (willPass) {
        printBlue(message);
        printGreen("----> passed");
	// printGreen("\n");

    }
    else {
        printBlue(message);
        printRed("----> did not pass");
	// printRed("\n");

    }
    return willPass;
}
inline bool test(bool bool_res)
{
    return test(bool_res, "working");
}

inline bool test(bool willPass, MString& message){
    return test(willPass,message.cstring());
}

