//
// Created by MERHAB NOUREDDINE on 02/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

//this only works on 64bit systems

#pragma once
#include <cassert>
#include "MString.h"
#include <climits>
#include <cfloat>
#include <cwchar>

enum MNumberType {MUChar,MUShortInt,MUInt,MULongInt,MULongLongInt,MChar,MLongChar,MShortInt,MInt,MLongInt,MLongLongInt,MFloat,MDouble,MLongDouble,MBool,MPointer,MNil};
class MNumber {
protected:
  MNumberType type = MNil;
  bool _isDecimal = false;
  bool _isUnsigned  =false;
  union{
    unsigned char uc;
    unsigned short int usi;
    unsigned int ui;
    unsigned long uli;
    unsigned long long ulli;
    char c;
    wchar_t lc;
    short int si;
    int i;
    long li;
    long long lli;
    float f;
    double d;
    long double ld;
    bool b;
    void* p;
  };
    unsigned long long int toMaxUInt();
    long long int toMaxInt();
    long double toMaxDouble();
public:
  /*--------------------------------------*/
  /*-------------constructors-------------*/
  /*--------------------------------------*/
  MNumber()=default;//tested
  
  MNumber(unsigned char uc){//tested
    this->uc = uc;
    type = MUChar;
      _isDecimal = false;
      _isUnsigned = true;
  }
  
  MNumber(unsigned short int usi){//tested
      _isDecimal = false;
      _isUnsigned = true;
    this->usi = usi;
    type = MUShortInt;
  }

  MNumber(unsigned int ui){//tested
      _isDecimal = false;
      _isUnsigned = true;
    this->ui = ui;
    type = MUInt;
  }

  MNumber(unsigned long uli){//tested
      _isDecimal = false;
      _isUnsigned = true;
    this->uli = uli;
    type = MULongInt;
  }
  
  MNumber(unsigned long long ulli){//tested
      _isDecimal = false;
      _isUnsigned = true;
    this->ulli = ulli;
    type = MULongLongInt;
  }

  MNumber(char c){//tested
      _isDecimal = false;
      _isUnsigned = false;
    this->c = c;
    type = MChar;
  }

  
  MNumber(wchar_t lc){//tested
      _isDecimal = false;
      _isUnsigned = false;
    this->lc = lc;
    type = MLongChar;
  }

  
  MNumber(short int si){//tested
      _isDecimal = false;
      _isUnsigned = false;
    this->si = si;
    type = MShortInt;
  }

  
  MNumber(int i){//tested
      _isDecimal = false;
      _isUnsigned = false;
    this->i = i;
    type = MInt;
  }

  MNumber(long li){//tested
      _isDecimal = false;
      _isUnsigned = false;
    this->li = li;
    type = MLongInt;
  }

  MNumber(long long lli){//tested
       _isDecimal = false;
      _isUnsigned = false;
    this->lli = lli;
    type = MLongLongInt;
  }

  MNumber(float f){//tested
      _isDecimal = true;
      _isUnsigned = false;
    this->f = f;
    type = MFloat;
  }

  MNumber(double d){//tested
      _isDecimal = true;
      _isUnsigned = false;
    this->d = d;
    type = MDouble;
  }

  MNumber(long double ld){//tested
      _isDecimal = true;
      _isUnsigned = false;
    this->ld = ld;
    type = MLongDouble;
  }

  explicit MNumber(bool b){//tested
      _isDecimal = false;
      _isUnsigned = true;
    this->b = b;
    type = MBool;
  }

  MNumber(void* p){//tested
      _isDecimal = false;
      _isUnsigned = true;
    this->p = p;
    type = MPointer;
  }

  ~MNumber(){

  }
  /*--------------------------------------*/
  unsigned char toUnsignedChar();//tested
  
  unsigned short int toUnsignedShortInt();//tested

  unsigned int toUnsignedInt();//tested

  unsigned long int toUnsignedLongInt();//tested

  unsigned long long int toUnsignedLongLongInt();//tested

  char toChar();//tested

  wchar_t toLongChar();//tested

  short int toShortInt();//tested

  int toInt();//tested
  
  long int toLongInt();//tested

  long long int toLongLongInt();//tested

  float toFloat();//tested

  double toDouble();//tested

  long double toLongDouble();//tested

  bool tooBool();//testeed

  void* toPointer();//tested

  bool isNil();//tested

  void toCString(char* strPtr,size_t size,int decimal=0);//tested

  MNumber operator+(MNumber number);//tested
  MNumber operator-(MNumber number);//tested
  MNumber operator*(MNumber number);//tested
  MNumber operator/(MNumber number);//tested
  MNumber operator%(MNumber number);//tested
    // prefix increment
  MNumber& operator++();//tested

    // postfix increment
  MNumber operator++(int);//tested

    // prefix decrement
  MNumber& operator--();//tested

    // postfix decrement
  MNumber operator--(int);//tested

  bool operator==(MNumber number);//tested

  bool  operator>(MNumber number);//tested

  bool  operator<(MNumber number);//tested

  bool  operator<=(MNumber number);//tested

  bool  operator>=(MNumber number);//tested



  bool isPointer();//tested

  void *getPointer();//tested

  MNumberType &getType();//tested

  bool  isUnsigned();
  bool isDecimal();
};
