//
// Created by MERHAB NOUREDDINE on 31/08/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#pragma once

#include <cassert>
#include "MSharedPtr.h"
#include <initializer_list>

static const size_t MVECTOR_SIZE = 250;

template<typename T>
class MVector {
    MSharedPtr<T> _data;
    size_t _size = -1;
    size_t _count = -1;
public:
    void setCount(size_t count) {
        _count = count;
    }

private:
    void detach(){
        if (*(_data._ref) == 1) return;
        auto dataTmp = MSharedPtr<T>(_size);
        for (size_t i=0 ;i<_count;i++){
            dataTmp._ptr[i] = _data._ptr[i];
        }
        _data = dataTmp;
    }

    void expand(){
        auto dataTmp = MSharedPtr<T>(_size*2);
        _size = _size*2;
        for (size_t i=0 ;i<_count;i++){
            dataTmp._ptr[i] = _data._ptr[i];
        }
        _data = dataTmp;
    }

public:
    explicit MVector(size_t size) : _data(size) {
        _count = 0;
        _size = size;
    }

    MVector() : MVector(MVECTOR_SIZE) {}

    MVector(const MVector &vec) {
        this->_data = vec._data;
        this->_size = vec._size;
        this->_count = vec._count;
    }

    MVector &operator=(const MVector &vec) {
        if (this == &vec) return *this;
        this->_data = vec._data;
        this->_size = vec._size;
        this->_count = vec._count;
        return *this;
    }

    T &operator[](size_t idx) {
        assert(_data._ptr != nullptr && idx>=0 && idx<_count);
        detach();
        return _data._ptr[idx];
    }

    const T &operator[](size_t idx) const {
        assert(_data._ptr != nullptr && idx>=0 && idx<_count);
        return _data._ptr[idx];
    }

    MVector & append(T data){
        assert(_data._ptr != nullptr);
        if(_count == _size )
            expand();
        else
            detach();
        _data._ptr[_count] = data;
        _count++;
        return *this;
    }

    MVector & insert(T data,size_t idx){
        assert(_data._ptr != nullptr and idx>=0 && idx<_count);
        if(_count == _size )
            expand();
        else
            detach();
        size_t i = _count;
        while (i>idx){
            _data._ptr[i] = _data._ptr[i-1];
            i--;
        }
        _data._ptr[i] = data;
        _count++;
        return *this;
    }

    MVector & insertAfterLast(T data){
        assert(_data._ptr != nullptr);
        append(data);
        return *this;
    }

    MVector & insertAtFirst(T data){
        assert(_data._ptr != nullptr);
        insert(data,0);
        return *this;
    }

    MVector & removeLast(){
        assert(_data._ptr != nullptr and _count>0);
        _count--;
        detach();
        return *this;
    }

    MVector & removeAt(size_t idx){
        assert(_data._ptr != nullptr and idx>=0 and idx<_count);
        detach();
        for (size_t i =idx;i<_count-1;i++){
            _data._ptr[i] = _data._ptr[i+1];
        }
        _count--;
        return *this;
    }

    MVector & removeFirst(){
        assert(_data._ptr != nullptr);
        detach();
        for (size_t i =0;i<_count-1;i++){
            _data._ptr[i] = _data._ptr[i+1];
        }
        _count--;
        return *this;
    }

    MVector & removeAll(){
        detach();
        delete  _data._ptr;
        _data._ptr =(T*) malloc(_size*(sizeof(T)));
        _count =0;
        return *this;
    }

    size_t size(){
        return _size;
    }

    size_t count(){
        return _count;
    }

    size_t find(T item){
        for (size_t i = 0; i < this->_count ; ++i) {
            if(_data._ptr[i]==item){
                return i;
            }
        }
        return -1;
    }

    MVector & removeItem(T item){
        size_t idx = find(item);
        if (idx != -1){
            removeAt(idx);
        }
        return *this;
    }

    T* getLastItem(){
        assert(_count >=1);
        return &(_data._ptr[_count-1]);
    }
    bool isEmpty(){
        return _count ==0;
    }


    MVector& append(std::initializer_list<T> args){
        for (auto &item:args) {
            this->append(item);
        }
        return *this;
    }

    MVector(std::initializer_list<T> args):MVector(){
        for (auto &item:args) {
            this->append(item);
        }
    }
};
