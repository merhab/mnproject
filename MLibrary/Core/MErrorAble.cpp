//
// Created by MERHAB NOUREDDINE on 11/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#include "MErrorAble.h"

void MErrorAble::setPassed() {
    _isSuccess = true;
}

void MErrorAble::setNotPassed(const char message[500]) {
    _isSuccess = false;
    copy(_error,message,500);
    assert(false);
}

bool MErrorAble::isPassed() {
    return _isSuccess;
}

char *MErrorAble::getError() {
    return _error;
}

MErrorAble::MErrorAble(const MErrorAble &errorAble) {
    this->_isSuccess = errorAble._isSuccess;
    copy(this->_error,errorAble._error);
}

MErrorAble &MErrorAble::operator=(const MErrorAble &errorAble) {
    if (this == &errorAble) return *this;
    this->_isSuccess = errorAble._isSuccess;
    copy(this->_error,errorAble._error);
    return *this;
}

MErrorAble::MErrorAble() {
    _error[0]=0;
    _isSuccess = true;
}
