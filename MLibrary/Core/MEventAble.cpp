//
// Created by mac on 27/08/2023.
//

#include "MEventAble.h"

void MEventAble::appendEventList(MEventList eventList) {
    for (size_t i = 0; i < this->events.count() ; ++i){
        if(this->events[i].name()==eventList.name()){
            assert(false);//we don`t want duplicated
        }
    }
    eventList.setReceiver(this);
    this->events.append(eventList);
}

void MEventAble::removeEventList(MString name) {
    for(int i=0 ;i<this->events.size();i++){
        if(this->events[i].name()==name){
            this->events.removeAt(i);
            return;
        }
    }
}

bool MEventAble::runCanEvents(int type, MAlphaNumList args) {
    for (size_t i = 0; i < this->events.count() ; ++i) {
        if (!this->events[i].runCanEvent(type,args))
            return false;
    }
    return true;
}

void MEventAble::runEvents(int type, MAlphaNumList args) {
    for (size_t i = 0; i < this->events.count() ; ++i) {
        this->events[i].runEvent(type,args);
    }
}
