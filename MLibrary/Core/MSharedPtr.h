//
// Created by MERHAB NOUREDDINE on 02/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#pragma once

#include <cstddef>
#include "test.h"

static int create =0;
static int rem =0;
static void incCreate(){
    create++;
    printf("%d MSharedPtr created\n",create);

}

static void incRem(){
    rem++;
    printf("%d MSharedPtr deleted\n",rem);
    if (create == rem){
        printYellow("All created MSharedPtr was deleted\n");
    }
}
template <typename T>
class MSharedPtr {
public:
    T* _ptr;
    int* _ref;

    explicit MSharedPtr(size_t size){
        _ref = new int;
        *_ref = 1;
        _ptr = (T*)malloc(sizeof(T)*size);
        incCreate();
    }

    MSharedPtr(){
        _ptr = nullptr;
        _ref = nullptr;
    }


    MSharedPtr(const MSharedPtr  &ptr){
        this->_ref = ptr._ref;
        (*(this->_ref))++;
        this->_ptr = ptr._ptr;
    }

     MSharedPtr(void* ptr){
         _ref = new int;
         *_ref = 1;
         this->_ptr = (T*)ptr;
         //incCreate();
    }

    MSharedPtr &operator=(const MSharedPtr  &ptr){
        if(this == &ptr) return *this;
        if (_ref) {
            if (*_ref > 1)
                (*_ref)--;
            else {
                delete _ptr;
                delete _ref;
                incRem();
            }
        }
        this->_ref = ptr._ref;
        this->_ptr = ptr._ptr;
        (*_ref)++;
        return *this;
    }

    ~MSharedPtr(){
        if (_ref) {
            if (*_ref > 1)
                (*_ref)--;
            else {
                delete _ptr;
                delete _ref;
                _ref = nullptr;
                _ptr = nullptr;
                incRem();
            }
        }
    }

    T* getPointer(){
        return _ptr;
    }

    T& getData(){
        return *_ptr;
    }

    bool isReadyToBeDeleted(){
        return (*_ref) == 1;
    }

    int getRef(){
        return *_ref;
    }

    void setData(T data){
        *_ptr = data;
    }
};
