//
// Created by mac on 31/08/2023.
//

#include "MVariant.h"

const char *MVariant::getTypeName() {
    if(!_isAlphaNum){
        return _typeName;
    } else if (!_alphaNum.isNumber()) {
        return MStringName;
    }else{
        switch (_alphaNum.getNumber().getType()) {
            case MInt:
                return IntName;
            case MLongInt:
                return LongName;
            case MLongLongInt:
                return LongLongName;
            case MFloat:
                return FloatName;
            case MDouble:
                return DoubleName;
            case MLongDouble:
                return LongDoubleName;
            case MBool:
                return BoolName;
            case MPointer:
                return PointerName;
            case MNil:
                return NilName;
            case MUChar:
                return MUCharName;
            case MUShortInt:
                return MUShortIntName;
            case MUInt:
                return MUIntName;
            case MULongInt:
                return MULongIntName;
            case MULongLongInt:
                return MULongLongIntName;
            case MChar:
                return MCharName;
            case MLongChar:
                return MLongCharName;
            case MShortInt: {
                return MShortIntName;
            }
        }
    }
}

MVariant::MVariant(const MVariant &variant) {
    this->_isAlphaNum = variant._isAlphaNum;
    if (!variant._isAlphaNum){
        this->_data = variant._data;
        this->_typeName = variant._typeName;
    } else{
        this->_alphaNum = variant._alphaNum;
    }
}

MVariant &MVariant::operator=(const MVariant &variant) {
    if(this == &variant) return *this;
    this->_isAlphaNum = variant._isAlphaNum;
    if (!variant._isAlphaNum){
        this->_data = variant._data;
        this->_typeName = variant._typeName;
    } else{
        this->_alphaNum = variant._alphaNum;
    }
    return *this;
}



















MVariant::MVariant() {
    this->_isAlphaNum = true;
    this->_alphaNum = MAlphaNum();
}

MVariant::MVariant(MAlphaNum alphaNum) {
    this->_isAlphaNum = true;
    this->_alphaNum = alphaNum;
}



MVariant::MVariant(MSharedData &variantData) {
    this->_isAlphaNum = false;
    this->_data = variantData;
    this->_typeName = variantData.getTypeName();
}



int MVariant::toInt() {
    if(this->_isAlphaNum ){
        return _alphaNum.toInt();
    }
    assert(false);
}

long MVariant::toLong() {
    if(this->_isAlphaNum ){
        return _alphaNum.toLongInt();
    }
    assert(false);
}

long long MVariant::toLongLong() {
    if(this->_isAlphaNum ){
        return _alphaNum.toLongLongInt();
    }
    assert(false);
}

float MVariant::toFloat() {
    if(this->_isAlphaNum ){
        return _alphaNum.toFloat();
    }
    assert(false);
}

double MVariant::toDouble() {
    if(this->_isAlphaNum ){
        return _alphaNum.toDouble();
    }
    assert(false);
}

long double MVariant::toLongDouble() {
    if(this->_isAlphaNum ){
        return _alphaNum.toLongDouble();
    }
    assert(false);
}

bool MVariant::toBool() {
    if(this->_isAlphaNum ){
        return _alphaNum.tooBool();
    }
    assert(false);
}

void *MVariant::toPointer() {
    if(this->_isAlphaNum ){
        return _alphaNum.toPointer();
    }
    assert(false);
}

bool MVariant::isNil() {
    if(_isAlphaNum){
        return this->_alphaNum.isNil();
    }else{
        return _data.getTypeName() == NilName;
    }
}

MString MVariant::toMString(char decimal) {
    if(this->_isAlphaNum ){
        if (_alphaNum.isNumber()){
            char str[500];
            _alphaNum.toCString(str,500,decimal);
            return str;
        } else{
            return _alphaNum.getMString();
        }

    } else {
        char * cs = _data.getCString();
        MString s(cs);
        delete cs;//must be deleted
        return s;
    }
}

//MVariant &MVariant::operator=(const MNumber &variant) {
//    this->operator=(MVariant(variant));
//    return *this;
//}





