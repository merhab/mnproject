//
// Created by mac on 30/08/2023.
//

#include "MString.h"
//static int inc=0;
//MString::MString():MString("") {
//
//}


MString::MString(size_t size)
: _sharedData(getMDataFuncs(), size) {
  //printf("%d%s\n",inc,"------>created");
    //this->_str = newCString(size);
    this->_size = size;
    this->_count = 0;
    //this->_str[0]=0;
    // this->id = inc;
    //inc++;
}

MString::MString(const char *str,bool willCloneData): _sharedData(getMDataFuncs(), size(str), (void*)str,willCloneData) {
  //printf("%d%s\n",inc,"------>created");
    //this->_sharedData.setData(newCString(str));
    this->_size = _sharedData.getSize();
    this->_count = _size;
    // this->id = inc;
    //inc++;

}

MString::MString(const MString &string)
: _sharedData(string._sharedData) {
_size = string._size;
_count = string._count;
}

MString& MString::operator=(const MString &str) {
    if(this==&str) return *this;
    //_delete(&this->_str);
    this->_sharedData = str._sharedData;
    this->_size = str._size;
    this->_count = str._size;
    return *this;
}

//MString& MString::operator=(const char *str) {
//    if(this->_str){
//        _delete( &_str);
//    }
//    this->_str = newCString(str);
//    this->_size = size(str);
//    this->_count = _size;
//    return *this;
//}

const char *MString::cstring() {
    return (const char *)this->getCString();
}

size_t MString::getSize() const {
    return _size;
}

size_t MString::getCount() const {
    return _count;
}


MString MString::operator+(const MString &string) {
    MString s(this->_size+string._size);
    char* c_result = s.getCString();
    char* c_this = this->getCString();
    char* c_added = (char*)string._sharedData._data;
    size_t i=0;
    for (; c_this[i];i++ ) {
        c_result[i]=c_this[i];
    }
    for (size_t j = 0; c_added[j] ; ++j) {
        c_result[i] = c_added[j];
        i++;
    }
    c_result[i]=0;
    s._count = this->_count + string._count;
    return s;
}

bool MString::operator==(const MString &string) {
    return isEqual(this->getCString(),(char*)string._sharedData._data);
}

int MString::toInt() {
    char *endPtr;
    long i = strtol(this->getCString(),&endPtr,10);
    if (endPtr == this->getCString()) assert(false);
    int a = (int) i;
    if(long (a) == i){
        setPassed();
        return a;
    } else{
        setNotPassed("this string dont has numeric value");
        assert(false);
    }
    return 0;
}

long MString::toLong() {
    char *endPtr;
    if(isNumeric()){
        if (isNumericDecimal()){
            if (getDecimalFraction()==0){
                auto n=toMaxDouble();
                if (n<=LONG_MAX){
                    setPassed();
                    return n;
                } else{
                    printf("%s\n","can`t convert because of boundary limits");
                    setNotPassed("can`t convert because of boundary limits");
                    assert(false);
                }
            } else{
                setNotPassed("can`t convert decimal to int");
                printf("%s\n","can`t convert decimal to int");
                assert(false);
            }
        } else if ((*this)[0]=='-'){
            auto n = toMaxInt();
            if (n<=LONG_MAX){
                setPassed();
                return n;
            } else{
                printf("%s\n","can`t convert because of boundary limits");
                setNotPassed("can`t convert because of boundary limits");
                assert(false);
            }
        } else{
            auto n = toMaxUint();
            if (n<=LONG_MAX){
                setPassed();
                return n;
            } else{
                setNotPassed("can`t convert because of boundary limits");
                printf("%s\n","can`t convert because of boundary limits");
                assert(false);
            }
        }
    }
    setNotPassed();
    assert(false);
    return 0;
}

long long MString::toLongLong() {
    long long i = toMaxInt();
    return i;
}

float MString::toFloat() {
    char *endPtr;
    float i = strtof(this->getCString(),&endPtr);
    if (endPtr == this->getCString()){
        setNotPassed(("cant convert \""+ *this + "\" to float ").getCString());
        assert(false);
    }
    setPassed();
    return i;
}

double MString::toDouble() {
    char *endPtr;
    double i = strtod(this->getCString(),&endPtr);
    if (endPtr == this->getCString()){
        setNotPassed(("cant convert \""+ *this + "\" to double ").getCString());
        assert(false);
    }
    setPassed();
    return i;
}

long double MString::toLongDouble() {
    char *endPtr;
    long double i = strtold(this->getCString(),&endPtr);
    if (endPtr == this->getCString()) {
        setNotPassed(("cant convert \""+ *this + "\" to long double ").getCString());
        assert(false);
    }
    return i;
}

MString MString::fromInt(int i) {
    MString s(250);
     snprintf(s.getCString(),250,"%d",i);
    return s;
}

MString MString::fromLong(long i) {
    MString s(250);
    snprintf(s.getCString(),250,"%ld",i);
    return s;
}

MString MString::fromLongLong(long long i) {
    MString s(250);
    snprintf(s.getCString(),250,"%lld",i);
    return s;
}

MString MString::fromFloat(float i,char decimal) {
    MString s(250);
    if (!decimal)
        snprintf(s.getCString(),250,"%f",i);
    else{
        char a[250];
        snprintf(a,250,"%s.%d%s","%",decimal,"f");
        snprintf(s.getCString(),250,a,i);
    }
    return s;
}

MString MString::fromDouble(double i, char decimal) {
    MString s(250);
    if (!decimal)
        snprintf(s.getCString(),250,"%lf",i);
    else{
        char a[250];
        snprintf(a,250,"%s.%d%s","%",decimal,"lf");
        snprintf(s.getCString(),250,a,i);
    }
    return s;
}

MString MString::fromLongDouble(long double i, char decimal) {
    MString s(500);
    if (!decimal)
        snprintf(s.getCString(),500,"%Lf",i);
    else{
        char a[250];
        snprintf(a,250,"%s.%d%s","%",decimal,"Lf");
        snprintf(s.getCString(),500,a,i);
    }
    return s;
}

void MString::_delete(char **str) {
    deleteCstring((void**)str);
}

const char &MString::operator[](size_t idx) const {
    return ((char*)this->_sharedData._data)[idx];
}

char &MString::operator[](size_t idx) {
    _sharedData.detach();
    return ((char*)this->_sharedData._data)[idx];
}

MDataFuncs MString::getMDataFuncs() {
    return {newCString, deleteCstring, cloneCString};
}

MString::MString(): _sharedData(getMDataFuncs(), 1) {
    this->getCString()[0]=0;
}

char *MString::getCString() {
    return (char *)this->_sharedData._data;
}

bool MString::isNumeric() {
    char* s = getCString();
    int coutMin=0;
    int countDot=0;
    for (size_t i = 0; s[i]; ++i) {
        if ((!(s[i]>= 45 && s[i]<=57))){
            return false;
        }
        if (s[i]=='.') countDot++;
        else if(s[i]=='-') {
            coutMin++;
            if(i!=0)
                return false;
        }
    }

    if(coutMin<=1 && countDot<=1)
        return true;
    else
        return false;
}

bool MString::isNumericNegative() {
    if(isNumeric() && getCString()[0]=='-')
        return true;
    else
        return false;
}

bool MString::isNumericDecimal() {
    char* s = getCString();
    int count =0;
    for (size_t i = 0; s[i] ; ++i) {
        if (s[i]=='.'){
            count++;
        }
    }
    if (count == 1)
        return true;
    else
        return false;
}

bool MString::operator>(const MString &string) {
    char * this_str = (char *)this->_sharedData._data;
    char * string_str = (char *)string._sharedData._data;
    size_t count ;
    if (this->_count <= string._count){
        count = this->_count;
    } else{
        count = string._count;
    }
    for (size_t i = 0; i < count; ++i) {
        if (this_str[i] == string[i])
            continue;
        else if(this_str[i] > string[i])
            return true;
        else
            return false;
    }
    return false;
}

bool MString::operator<(const MString &string) {
    char * this_str = (char *)this->_sharedData._data;
    char * string_str = (char *)string._sharedData._data;
    size_t count ;
    if (this->_count <= string._count){
        count = this->_count;
    } else{
        count = string._count;
    }
    for (size_t i = 0; i < count; ++i) {
        if (this_str[i] == string[i])
            continue;
        else if(this_str[i] < string[i])
            return true;
        else
            return false;
    }
    return false;
}

bool MString::isNumericUnsigned() {
    if(((isNumeric()) && (getCString()[0]!='-')))
        return true;
    else
        return false;
}

unsigned long long int MString::toMaxUint() {
    if (!isNumeric()){
        setNotPassed(("cant convert \""+ *this + "\" to unsigned long long ").getCString());
        assert(false);
    }else if (this->getCString()[0]=='-'){
        setNotPassed(("cant convert \""+ *this + "\" to unsigned long long ").getCString());
        assert(false);
    }else{
        unsigned long long int ulli=0;
        for (size_t i = 0; (*this)[i] ; ++i) {
            ulli = ulli *10 + ((*this)[i]-'0');

        }
        setPassed();
        return ulli;
    }
    setNotPassed();
    return 0;
}

long long int MString::toMaxInt() {
    if (!isNumeric()){
        setNotPassed(("cant convert \""+ *this + "\" to long long ").getCString());
        assert(false);
    }else{
        long long int lli=0;
        for (size_t i = 0; (*this)[i] ; ++i) {
            lli = lli *10 + ((*this)[i]-'0');
        }
        setPassed();
        return lli;
    }
    setNotPassed(("cant convert \""+ *this + "\" to unsigned long long ").getCString());
    assert(false);
    return 0;
}

long double MString::toMaxDouble() {
    if (!isNumeric()){
        setNotPassed(("cant convert \""+ *this + "\" to  long double ").getCString());
        assert(false);
    }else{
        bool isNegative = false;
        long double fraction  =1;
        bool isFraction = false;
        size_t i = 0;
        if((*this)[0]==-1){
           isNegative = true;
           i++;
        }

        long double ld=0.0;
        for (; (*this)[i] ; ++i) {
            if((*this)[i] == '.') {
                isFraction = true;
            } else{
               if(isFraction){
                   fraction/=10.0;
                   ld = ld + (((*this)[i] - '0')*fraction);
               } else{
                   ld = ld * 10 + ((*this)[i] - '0');
               }
            }
        }
        setPassed();
        if (isNegative)
            return -ld;
        else
            return ld;
    }
    setNotPassed();
    assert(false);
    return 0;
}

bool MString::toBool() {
    setPassed();
    if(*this == "true"){
        return true;
    } else if(*this == "false"){
        return false;
    } else if(this->isNumeric()){
        if(*this == "0"){
            return false;
        } else{return true;}
    } else {
        setNotPassed(("cant convert \""+ *this + "\" to bool ").getCString());
        assert(false);
    }
    return false;
}

unsigned long long int MString::getDecimalFraction() {
    bool dotFound= false;
    size_t i = 0;
    for (; i <_count ; ++i) {
        if (this->getCString()[i]=='.')
            break;
    }
    i++;
    unsigned long long int ld=0;
    for (;i <_count ; ++i) {
        ld = ld * 10 + ((*this)[i] - '0');
    }
    return ld;
}

MString &MString::operator+=(MString str) {
    *this = *this + str;
    return *this;
}

bool MString::operator!=(MString str) {
    return !(*this == str);
}


MString operator+(const char str[], MString string) {
    return MString(str) + string;
}

