//
// Created by MERHAB NOUREDDINE on 11/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#pragma once
#include "CString.h"

class MErrorAble {
protected:
    char _error[500];
    bool _isSuccess;
public:
    void setPassed();
    void setNotPassed(const char message[500]=0);
    bool isPassed();
    char* getError();
    MErrorAble(const MErrorAble &errorAble);
    MErrorAble& operator=(const MErrorAble& errorAble);
    MErrorAble();
};
