//
// Created by MERHAB NOUREDDINE on 15/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#include "MStringList.h"

MString &MStringList::operator[](size_t idx) {
    return _strings[idx];
}

const MString &MStringList::operator[](size_t idx) const {
    return _strings[idx];
}

size_t MStringList::count() {
    return _strings.count();
}

size_t MStringList::size() {
    return _strings.size();
}

MStringList& MStringList::append(MString val) {
    _strings.append(val);
    return *this;
}

MStringList& MStringList::append(MStringList strList) {
    for (size_t i = 0; i < strList.count(); ++i) {
        this->append(strList[i]);
    }
    return *this;
}

MStringList::MStringList(std::initializer_list<MString> args) {
    for (auto& item:args) {
        _strings.append(item);
    }
}

MStringList &MStringList::append(std::initializer_list<MString> args) {
    for (auto& item:args) {
        _strings.append(item);
    }
    return *this;
}

MString MStringList::join(const char *string) {
    assert(!this->isEmpty());
    size_t str_sub_size = ::size(string);
    size_t charCount = getCharCount() + (str_sub_size*(count()-1));
    char* str = (char*) newCString(charCount);
    str[charCount]  =0;
    size_t i=0;
    for (size_t j = 0; j < this->count() ; ++j) {
        char* str_sub = (*this)[j].getCString();
        for (size_t k = 0; k < (*this)[j].getCount() ; ++k) {
            str[i]=str_sub[k];
            i++;
        }
        if (i<charCount){
            for (size_t k = 0; k < str_sub_size ; ++k) {
                str[i]=string[k];
                i++;
            }
        }
    }
    return {str, false};
//    if (this->count() == 1) return (*this)[0];
//    MString result;
//    result = (*this)[0];
//    for (size_t i = 1; i < this->count() ; ++i) {
//        result += string + (*this)[i];
//    }
//    return result;
}

bool MStringList::isEmpty() {
    return count() == 0;
}

size_t MStringList::getCharCount() {
    size_t count  =0;
    for (size_t i = 0; i < this->count() ; ++i) {
        count += (*this)[i].getCount();
    }
    return count;
}
