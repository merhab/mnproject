//
// Created by MERHAB NOUREDDINE on 02/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#include "MNumbers.h"
#include <cassert>
unsigned long long int MNumber::toMaxUInt(){
    switch (this->type) {

        case MUChar:
            return (long long int) uc;
        case MUShortInt:
            return (long long int) usi;
        case MUInt:
            return (long long int) ui;
        case MULongInt:
            return (long long int) uli;
        case MULongLongInt:
            return ulli;
        case MChar:
            if(c>=0)
                return (unsigned long long int ) c;
            else {
                printf("%s/n", "can`t convert from signed number to unsigned");
                assert(false);
            }
        case MLongChar:
            if(lc>=0)
                return (unsigned long long int ) lc;
            else {
                printf("%s/n", "can`t convert from signed number to unsigned");
                assert(false);
            }
        case MShortInt:
            if(si>=0)
                return (unsigned long long int ) si;
            else {
                printf("%s/n", "can`t convert from signed number to unsigned");
                assert(false);
            }
        case MInt:
            if(i>=0)
                return (unsigned long long int ) i;
            else {
                printf("%s/n", "can`t convert from signed number to unsigned");
                assert(false);
            }
        case MLongInt:
            if(li>=0)
                return (unsigned long long int ) li;
            else {
                printf("%s/n", "can`t convert from signed number to unsigned");
                assert(false);
            }
        case MLongLongInt:
            if(lli>=0)
                return (unsigned long long int ) lli;
            else {
                printf("%s/n", "can`t convert from signed number to unsigned");
                assert(false);
            }
        case MFloat:
            if((unsigned long long int)f == f){
                    return (unsigned long long int ) f;
            }
            else {
                printf("%s/n", "can`t convert from decimal number to unsigned");
                assert(false);
            }
        case MDouble:
            if((unsigned long long int)d == d){
                return (unsigned long long int ) d;
            }
            else {
                printf("%s/n", "can`t convert from decimal number to unsigned");
                assert(false);
            }
        case MLongDouble:
            if((unsigned long long int)ld == ld){
                return (unsigned long long int ) ld;
            }
            else {
                printf("%s/n", "can`t convert from decimal number to unsigned");
                assert(false);
            }
        case MBool:
            if (b)
                return 1;
            else
                return 0;
        case MPointer:
            return (unsigned long long int ) p;
        case MNil: {
            printf("%s/n", "nil value err||");
            assert(false);
        }
    }
}

long long int MNumber::toMaxInt() {
    switch (this->type) {

        case MUChar:
            return (long long int) uc;
        case MUShortInt:
            if((long long int) usi == usi)
                return (long long int) usi;
            else{
                printf("%s/n", "can`t convert from signed number to unsigned");
                assert(false);
            }
        case MUInt:
            if((long long int) ui == ui)
                return (long long int) ui;
            else{
                printf("%s/n", "can`t convert from signed number to unsigned");
                assert(false);
            }
        case MULongInt:
            if((long long int) uli == uli)
                return (long long int) uli;
            else{
                printf("%s/n", "can`t convert from signed number to unsigned");
                assert(false);
            }
        case MULongLongInt:
            if((long long int) ulli == ulli)
                return (long long int) ulli;
            else{
                printf("%s/n", "can`t convert from signed number to unsigned");
                assert(false);
            }
        case MChar:
            return c;
        case MLongChar:
            return lc;
        case MShortInt:
            return si;
        case MInt:
            return i;
        case MLongInt:
            return li;
        case MLongLongInt:
            return lli;
        case MFloat:
            if((long long int) f == f)
                return (long long int) f;
            else{
                printf("%s/n", "can`t convert float to int");
                assert(false);
            }
        case MDouble:
            if((long long int) d == d)
                return (long long int) d;
            else{
                printf("%s/n", "can`t convert double to int");
                assert(false);
            }
        case MLongDouble:
            if((long long int) ld == ld)
                return (long long int) ld;
            else{
                printf("%s/n", "can`t convert long double to int");
                assert(false);
            }
        case MBool:
            if (b)
                return 1;
            else
                return 0;
        case MPointer:
            return (long long int ) p;
        case MNil: {
            printf("%s/n", "nil value err||");
            assert(false);
        }
    }
}

long double MNumber::toMaxDouble() {
    switch (this->type) {

        case MUChar:
            return (long double) uc;
        case MUShortInt:
            if((long double) usi == usi)
                return (long double) usi;
            else{
                printf("%s/n", "can`t convert from signed number to unsigned");
                assert(false);
            }
        case MUInt:
            if((long double) ui == ui)
                return (long double) ui;
            else{
                printf("%s/n", "can`t convert from signed number to unsigned");
                assert(false);
            }
        case MULongInt:
            if((long double)uli == uli)
                return (long double)uli;
            else{
                printf("%s/n", "can`t convert from signed number to unsigned");
                assert(false);
            }
        case MULongLongInt:
            if((long double) ulli == ulli)
                return (long double) ulli;
            else{
                printf("%s/n", "can`t convert from signed number to unsigned");
                assert(false);
            }
        case MChar:
            if((long double) c == c)
                return (long double) c;
            else{
                printf("%s/n", "can`t convert from int number to decimal");
                assert(false);
            }
        case MLongChar:
            if((long double) lc == lc)
                return (long double) lc;
            else{
                printf("%s/n", "can`t convert from int number to decimal");
                assert(false);
            }
        case MShortInt:
            if((long double) si == si)
                return (long double) si;
            else{
                printf("%s/n", "can`t convert from int number to decimal");
                assert(false);
            }
        case MInt:
            if((long double) i == i)
                return (long double) i;
            else{
                printf("%s/n", "can`t convert from int number to decimal");
                assert(false);
            }
        case MLongInt:
            if((long double) li == li)
                return (long double) li;
            else{
                printf("%s/n", "can`t convert from int number to decimal");
                assert(false);
            }
        case MLongLongInt:
            if((long double) lli == lli)
                return (long double) lli;
            else{
                printf("%s/n", "can`t convert from int number to decimal");
                assert(false);
            }
        case MFloat:
            return f;
        case MDouble:
            return d;
        case MLongDouble:
            return ld;
        case MBool:
            if (b)
                return 1;
            else
                return 0;
        case MPointer:
        {
            printf("%s/n", "cant convert a pointer to a decimal");
            assert(false);
        }
        case MNil: {
            printf("%s/n", "nil value err||");
            assert(false);
        }
    }
}
unsigned char MNumber::toUnsignedChar() {
    unsigned long long int ulli=toMaxUInt();
    if (ulli <= UCHAR_MAX){
        return (unsigned char) ulli;
    }else{
        printf("%s\n","err|| of type conversion");
        assert(false);
    }
}

unsigned short int MNumber::toUnsignedShortInt() {
    unsigned long long int ulli=toMaxUInt();
    if (ulli <= USHRT_MAX){
        return (unsigned short int) ulli;
    }else{
        printf("%s\n","err|| of type conversion");
        assert(false);
    }
}


unsigned int MNumber::toUnsignedInt() {
    unsigned long long int ulli=toMaxUInt();
    if (ulli <= UINT_MAX){
        return (unsigned int) ulli;
    }else{
        printf("%s\n","err|| of type conversion");
        assert(false);
    }
}


unsigned long int MNumber::toUnsignedLongInt() {
    unsigned long long int ulli=toMaxUInt();
    if (ulli <= ULONG_MAX){
        return (unsigned long int) ulli;
    }else{
        printf("%s\n","err|| of type conversion");
        assert(false);
    }
}

unsigned long long int MNumber::toUnsignedLongLongInt() {
    return toMaxUInt();
}

char MNumber::toChar() {
 long long int lli = toMaxInt();
    if (lli <= CHAR_MAX){
        return (char) lli;
    }else{
        printf("%s\n","err|| of type conversion");
        assert(false);
    }
}

wchar_t MNumber::toLongChar() {
    long long int lli = toMaxInt();
    if (lli <= WCHAR_MAX){
        return (wchar_t) lli;
    }else{
        printf("%s\n","err|| of type conversion");
        assert(false);
    }
}

short int MNumber::toShortInt() {
    long long int lli = toMaxInt();
    if (lli <= SHRT_MAX){
        return (short int) lli;
    }else{
        printf("%s\n","err|| of type conversion");
        assert(false);
    }
}

int MNumber::toInt() {
    long long int lli = toMaxInt();
    if (lli <= INT_MAX){
        return (int) lli;
    }else{
        printf("%s\n","err|| of type conversion");
        assert(false);
    }
}

long int MNumber::toLongInt() {
    long long int lli = toMaxInt();
    if (lli <= LONG_MAX){
        return (long int) lli;
    }else{
        printf("%s\n","err|| of type conversion");
        assert(false);
    }
}

long long int MNumber::toLongLongInt() {
    long long int lli = toMaxInt();
    if (lli <= LLONG_MAX){
        return (long long int) lli;
    }else{
        printf("%s\n","err|| of type conversion");
        assert(false);
    }
}

float MNumber::toFloat() {
    long double ld = toMaxDouble();
    if (ld <= FLT_MAX){
        return (float) ld;
    }else{
        printf("%s\n","err|| of type conversion");
        assert(false);
    }
}

double MNumber::toDouble() {
    long double ld = toMaxDouble();
    if (ld <= DBL_MAX){
        return (double) ld;
    }else{
        printf("%s\n","err|| of type conversion");
        assert(false);
    }
}

long double MNumber::toLongDouble() {
    return toMaxDouble();
}

bool MNumber::tooBool() {
    switch (type) {

        case MUChar:
            return uc;
        case MUShortInt:
            return usi;
        case MUInt:
            return ui;
        case MULongInt:
            return uli;
        case MULongLongInt:
            return ulli;
        case MChar:
            return c;
        case MLongChar:
            return lc;
        case MShortInt:
            return si;
        case MInt:
            return i;
        case MLongInt:
            return li;
        case MLongLongInt:
            return lli;
        case MFloat:
            return f !=0;
        case MDouble:
            return d!=0;
        case MLongDouble:
            return ld != 0;
        case MBool:
            return b;
        case MPointer:
            return p;
        case MNil:
            assert(false);
    }
}

void* MNumber::toPointer() {
  if(type == MPointer)
    return p;
  else
    assert(false);
}

bool MNumber::isNil() {
  return type == MNil;
}

void MNumber::toCString(char *strPtr,size_t size,int decimal) {
    switch (type) {

        case MUChar:
            snprintf(strPtr,size,"%hhu",uc);
            break;
        case MUShortInt:
            snprintf(strPtr,size,"%hu",usi);
            break;
        case MUInt:
            snprintf(strPtr,size,"%u",ui);
            break;
        case MULongInt:
            snprintf(strPtr,size,"%lu",uli);
            break;
        case MULongLongInt:
            snprintf(strPtr,size,"%llu",ulli);
            break;
        case MChar:
            snprintf(strPtr,size,"%hhd",c);
            break;
        case MLongChar:
            snprintf(strPtr,size,"%d",lc);
            break;
        case MShortInt:
            snprintf(strPtr,size,"%hd",si);
            break;
        case MInt:
            snprintf(strPtr,size,"%d",i);
            break;
        case MLongInt:
            snprintf(strPtr,size,"%ld",li);
            break;
        case MLongLongInt:
            snprintf(strPtr,size,"%lld",lli);
            break;
        case MFloat:
            if (!decimal)
                snprintf(strPtr,size,"%f",f);
            else{
                char a[250];
                snprintf(a,250,"%s.%d%s","%",decimal,"f");
                snprintf(strPtr,size,a,f);
            }
            break;
        case MDouble:
            if (!decimal)
                snprintf(strPtr,size,"%lf",d);
            else{
                char a[250];
                snprintf(a,250,"%s.%d%s","%",decimal,"lf");
                snprintf(strPtr,size,a,d);
            }
            break;
        case MLongDouble:
            if (!decimal)
                snprintf(strPtr,size,"%Lf",ld);
            else{
                char a[250];
                snprintf(a,250,"%s.%d%s","%",decimal,"Lf");
                snprintf(strPtr,size,a,ld);
            }
            break;
        case MBool:
            if(b)
                snprintf(strPtr,size,"%s","true");
            else
                snprintf(strPtr,size,"%s", "false");
            break;
        case MPointer:
            snprintf(strPtr,size,"%lld",reinterpret_cast<long long int>(p));
            break;
        case MNil:
            snprintf(strPtr,size,"%s","nil");
            break;
    }
}

MNumber MNumber::operator+(MNumber number) {
    if(type == MBool || type == MPointer || type == MNil){
        assert(false);
    }
    if(number.type == MBool || number.type == MPointer || number.type == MNil){
        assert(false);
    }
    if(_isUnsigned && number._isUnsigned){
        return toUnsignedLongLongInt() + number.toUnsignedLongLongInt();
    }
    else if(_isDecimal && number._isDecimal){
        return toLongDouble() + number.toLongDouble();
    }
    else if(!_isDecimal && number._isDecimal){
        return (long double)toLongLongInt() + number.toLongDouble();
    }
    else if(_isDecimal && !number._isDecimal){
        return toLongDouble() + (long double)number.toLongLongInt();
    }
    else if(!_isDecimal && !number._isDecimal){
        return toLongLongInt() + number.toLongLongInt();
    } else{
        return MNumber();
    }

}

MNumber MNumber::operator-(MNumber number) {
    if(type == MBool || type == MPointer || type == MNil){
        assert(false);
    }
    if(number.type == MBool || number.type == MPointer || number.type == MNil){
        assert(false);
    }
    if(_isUnsigned && number._isUnsigned){
        return toUnsignedLongLongInt() - number.toUnsignedLongLongInt();
    }
    else if(_isDecimal && number._isDecimal){
        return toLongDouble() - number.toLongDouble();
    }
    else if(!_isDecimal && number._isDecimal){
        return (long double)toLongLongInt() - number.toLongDouble();
    }
    else if(_isDecimal && !number._isDecimal){
        return toLongDouble() - (long double)number.toLongLongInt();
    }
    else if(!_isDecimal && !number._isDecimal){
        return toLongLongInt() - number.toLongLongInt();
    } else{
        return MNumber();
    }
}

MNumber MNumber::operator*(MNumber number) {
    if(type == MBool || type == MPointer || type == MNil){
        assert(false);
    }
    if(number.type == MBool || number.type == MPointer || number.type == MNil){
        assert(false);
    }
    if(_isUnsigned && number._isUnsigned){
        return toUnsignedLongLongInt() * number.toUnsignedLongLongInt();
    }
    else if(_isDecimal && number._isDecimal){
        return toLongDouble() * number.toLongDouble();
    }
    else if(!_isDecimal && number._isDecimal){
        return (long double)toLongLongInt() * number.toLongDouble();
    }
    else if(_isDecimal && !number._isDecimal){
        return toLongDouble() * (long double)number.toLongLongInt();
    }
    else if(!_isDecimal && !number._isDecimal){
        return toLongLongInt() * number.toLongLongInt();
    } else{
        return MNumber();
    }
}

MNumber MNumber::operator/(MNumber number) {
    if(type == MBool || type == MPointer || type == MNil){
        assert(false);
    }
    if(number.type == MBool || number.type == MPointer || number.type == MNil){
        assert(false);
    }
    if(_isUnsigned && number._isUnsigned){
        return toUnsignedLongLongInt() / number.toUnsignedLongLongInt();
    }
    else if(_isDecimal && number._isDecimal){
        return toLongDouble() / number.toLongDouble();
    }
    else if(!_isDecimal && number._isDecimal){
        return (long double)toLongLongInt() / number.toLongDouble();
    }
    else if(_isDecimal && !number._isDecimal){
        return toLongDouble() / (long double)number.toLongLongInt();
    }
    else if(!_isDecimal && !number._isDecimal){
        return toLongLongInt() / number.toLongLongInt();
    } else{
        return MNumber();
    }
}

MNumber MNumber::operator%(MNumber number) {
    if(type == MBool || type == MPointer || type == MNil){
        assert(false);
    }
    if(number.type == MBool || number.type == MPointer || number.type == MNil){
        assert(false);
    }
    if(_isUnsigned && number._isUnsigned){
        return toUnsignedLongLongInt() % number.toUnsignedLongLongInt();
    }
    else if(!_isDecimal && !number._isDecimal){
        return toLongLongInt() % number.toLongLongInt();
    } else{
        assert(false);
    }
}

MNumber &MNumber::operator++() {
    switch (type) {
        case MBool:
        case MPointer:
        case MNil:
            assert(false);
        case MUChar:
            uc++;
            break;
        case MUShortInt:
            usi++;
            break;
        case MUInt:
            ui++;
            break;
        case MULongInt:
            uli++;
            break;
        case MULongLongInt:
            ulli++;
            break;
        case MChar:
            c++;
            break;
        case MLongChar:
            lc++;
            break;
        case MShortInt:
            si++;
            break;
        case MInt:
            i++;
            break;
        case MLongInt:
            li++;
            break;
        case MLongLongInt:
            lli++;
            break;
        case MFloat:
            f++;
            break;
        case MDouble:
            d++;
            break;
        case MLongDouble:
            ld++;
            break;
    }
    return *this; // return new value by reference
}

MNumber MNumber::operator++(int) {
    MNumber old = *this; // copy old value
    operator++();  // prefix increment
    return old;    // return old value
}

MNumber &MNumber::operator--() {
    switch (type) {
        case MBool:
        case MPointer:
        case MNil:
            assert(false);
        case MUChar:
            uc--;
            break;
        case MUShortInt:
            usi--;
            break;
        case MUInt:
            ui--;
            break;
        case MULongInt:
            uli--;
            break;
        case MULongLongInt:
            ulli--;
            break;
        case MChar:
            c--;
            break;
        case MLongChar:
            lc--;
            break;
        case MShortInt:
            si--;
            break;
        case MInt:
            i--;
            break;
        case MLongInt:
            li--;
            break;
        case MLongLongInt:
            lli--;
            break;
        case MFloat:
            f--;
            break;
        case MDouble:
            d--;
            break;
        case MLongDouble:
            ld--;
            break;
    }
    return *this; // return new value by reference
}

MNumber MNumber::operator--(int) {
    MNumber old = *this; // copy old value
    operator--();  // prefix decrement
    return old;    // return old value
}

bool MNumber::operator==(MNumber number) {
    if(type == MNil){
        assert(false);
    }
    if(type == MPointer && number.type == MPointer){
        return p == number.p;
    }else if(type ==MBool && number.type == MBool){
        return b==number.b;
    }else if(_isUnsigned && number._isUnsigned){
        return toUnsignedLongLongInt() == number.toUnsignedLongLongInt();
    }
    else if(_isDecimal && number._isDecimal){
        return toLongDouble() == number.toLongDouble();
    }
    else if(!_isDecimal && number._isDecimal){
        return (long double)toLongLongInt() == number.toLongDouble();
    }
    else if(_isDecimal && !number._isDecimal){
        return toLongDouble() == (long double)number.toLongLongInt();
    }
    else if(!_isDecimal && !number._isDecimal){
        return toLongLongInt() == number.toLongLongInt();
    } else{
        return false;
    }
}

bool MNumber::operator>(MNumber number) {
    if(type == MNil){
        assert(false);
    }
    if(type == MPointer && number.type == MPointer){
        return p > number.p;
    }else if(type ==MBool && number.type == MBool){
        return b > number.b;
    }else if(_isUnsigned && number._isUnsigned){
        return toUnsignedLongLongInt() > number.toUnsignedLongLongInt();
    }
    else if(_isDecimal && number._isDecimal){
        return toLongDouble() > number.toLongDouble();
    }
    else if(!_isDecimal && number._isDecimal){
        return (long double)toLongLongInt() > number.toLongDouble();
    }
    else if(_isDecimal && !number._isDecimal){
        return toLongDouble() > (long double)number.toLongLongInt();
    }
    else if(!_isDecimal && !number._isDecimal){
        return toLongLongInt() > number.toLongLongInt();
    } else{
        return false;
    }
}

bool MNumber::operator<(MNumber number) {
    if(type == MNil){
        assert(false);
    }
    if(type == MPointer && number.type == MPointer){
        return p < number.p;
    }else if(type ==MBool && number.type == MBool){
        return b < number.b;
    }else if(_isUnsigned && number._isUnsigned){
        return toUnsignedLongLongInt() < number.toUnsignedLongLongInt();
    }
    else if(_isDecimal && number._isDecimal){
        return toLongDouble() < number.toLongDouble();
    }
    else if(!_isDecimal && number._isDecimal){
        return (long double)toLongLongInt() < number.toLongDouble();
    }
    else if(_isDecimal && !number._isDecimal){
        return toLongDouble() < (long double)number.toLongLongInt();
    }
    else if(!_isDecimal && !number._isDecimal){
        return toLongLongInt() < number.toLongLongInt();
    } else{
        return false;
    }
}

bool MNumber::isPointer() {
    return this->type == MPointer;
}

void *MNumber::getPointer() {
    if(type == MPointer)
        return p;
    else{
        printf("%s\n","this is not a pointer");
        assert(false);
    }
}

MNumberType &MNumber::getType() {
    return type;
}

bool MNumber::operator<=(MNumber number) {
    return *this < number || *this == number;
}

bool MNumber::operator>=(MNumber number) {
    return *this > number || *this == number;
}

bool MNumber::isUnsigned() {
    return _isUnsigned;
}

bool MNumber::isDecimal() {
    return _isDecimal;
}









