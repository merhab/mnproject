//
// Created by MERHAB NOUREDDINE on 15/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#include "MAlphaNumList.h"

MAlphaNum &MAlphaNumList::operator[](size_t idx) {
    return _list[idx];
}

const MAlphaNum &MAlphaNumList::operator[](size_t idx) const {
    return _list[idx];
}

size_t MAlphaNumList::count() {
    return _list.count();
}

size_t MAlphaNumList::size() {
    return _list.size();
}

MAlphaNumList& MAlphaNumList::append(MAlphaNum val) {
    _list.append(val);
    return *this;
}

MAlphaNumList::MAlphaNumList(std::initializer_list<MAlphaNum> items) {
    _list.append(items);
}

MAlphaNumList &MAlphaNumList::append(std::initializer_list<MAlphaNum> items) {
    _list.append(items);
    return *this;
}

bool MAlphaNumList::isEmpty() {
    return this->count()==0;
}
