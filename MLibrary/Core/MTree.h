//
// Created by MERHAB NOUREDDINE on 06/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//
#pragma once
#include "MVector.h"

template<typename T>
class MBNode{
public:
    T _value;
    MBNode* _leftNode = nullptr;
    MBNode* _rightNode = nullptr;
//    MBNode() {
//
//    }
    MBNode(T value,MBNode* leftNode= nullptr,MBNode* rightNode = nullptr){
        _value = value;
        _leftNode = leftNode;
        _rightNode = rightNode;
    }
    MBNode(const MBNode &node){
        _value = node._value;
        _leftNode = node._leftNode;
        _rightNode = node._rightNode;
    }

    T getValue() const {
        return _value;
    }

    void setValue(T value) {
        _value = value;
    }

    MBNode *getLeftNode() const {
        return _leftNode;
    }

    void setLeftNode(MBNode *leftNode) {
        _leftNode = leftNode;
    }

    MBNode *getRightNode() const {
        return _rightNode;
    }

    void setRightNode(MBNode *rightNode) {
        _rightNode = rightNode;
    }

    MBNode& operator=(const MBNode &node){
        if (this == &node) return *this;
        _value = node._value;
        _leftNode = node._leftNode;
        _rightNode = node._rightNode;
        return *this;
    }

    ~MBNode(){

    }
};

template<typename T>
class MBTree{
public:
    typedef MBNode<T> MBNode;
    MVector<MBNode> host;
    MBNode* _root;

    MBNode *getRoot() const {
        return _root;
    }

    void setRoot(MBNode *root) {
        MBTree::_root = root;
    }

    MBTree(T rootVal){
        host.append(MBNode(rootVal));
        _root = &(host[0]);
    }
    MBTree(const MBTree& tree){
        this->host = tree.host;
        this->root = tree.root;
    }
    MBTree& operator=(const MBTree& tree){
        if(this==&tree) return *this;
        this->host = tree.host;
        this->root = tree.root;
        return *this;
    } ;
    MBNode* newNode(T val){
        this->host.append(MBNode(val));
        return &(host[host.count()-1]);
    }

    MBNode* setLeftNode(T val, MBNode* parent){
        auto nd = newNode(val);
        parent->setLeftNode(nd);
        return nd;
    }

    MBNode* setRightNode(T val, MBNode* parent){
        auto nd = newNode(val);
        parent->setRightNode(nd);
        return nd;
    }
};