//
// Created by mac on 30/08/2023.
//
#include "cstring_t.h"
#include "MString_t.h"
#include "MVariant_t.h"
#include "MNumber_t.h"
#include "MVector_t.h"
#include "MTree_t.h"
#include "MSharedPointer_t.h"
#include "MStringList_t.h"
//int inc =0;
//int del = 0;
int main(int args,char** arg){
    unsigned long start = clock();
    for (int i = 0; i <1 ; ++i){
        bool pass = true;

        pass = pass && (mStringList_t());
        if (pass)
            printGreen("MSTRINGLIST PASSED");

        if (cstring_t()) {
            printGreen("CSTRING PASSED TH TEST\n");
        } else
            pass = false;
        if (mString_t()) {
            printGreen("MSTRING PASSED TH TEST\n");
        } else
            pass = false;
        MString s(MString::fromInt(inc) + MString(" cstring created\n")
                  + MString::fromInt(del) + MString(" cstring deleted\n"));
        printMag(s.cstring());
        printf("%d cstring created\n %d cstring deleted\n", inc, del);
        if (mVariant_t()) {
            printGreen("MVARIANT PASSED THE TEST\n");
        } else
            pass = false;
        if (mNumbers_t()) {
            printGreen("MNUMBER PASSED THE TEST\n");
        } else
            pass = false;
        if (mVector_t()) {
            printGreen("MVECTOR PASSED THE TEST\n");
        } else
            pass = false;

       pass = pass && (mTree_t());
        if (pass)
            printGreen("MTree PASSED THE TEST\n");
        pass = pass && (mSharedPointer_t());
        if (pass)
            printGreen("MSHREDPTR PASSED THE TEST\n");

        if (pass) {
            printYellow("all passed successfully\n");
        } else
            printRed("some tests did not pass\n");


    }
    unsigned long end = clock();//Now check what amount of ticks we have now.
//To get the time, just subtract start from end, and divide by CLOCKS_PER_SEC.
std::cout << arg[0]<<"\n";
    std::cout << "it took " << end - start << "ticks, or " << ((float)end -(float) start)/CLOCKS_PER_SEC << "seconds." << std::endl;
    return 0;
}