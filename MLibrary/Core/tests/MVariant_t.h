//
// Created by MERHAB on 31/08/2023.
//

#pragma once
#include "MVariant.h"
#include "test.h"
inline bool mVariant_t(){
  printMag(
	   "-----------------------------------------\n"
	   "-----------TESTING MVARIANT--------------\n"
	   "-----------------------------------------\n"
	   );
  bool res = true;
  MVariant var(10);
    res = res &&  test(var.toInt()==10,"MVariant(int)");
    res = res &&  test(var.toLong()==(long)10,"MVariant(int).toLong()");
    res = res &&  test(var.toLongLong()==(long long)10,"MVariant(int).toLongLong()");
    res = res &&  test(var.toMString()=="10","MVariant(int).toString()");
  var = (long)10;
    res = res &&   test(var.toLong()==(long)10,"MVariant(long).toLong()");
    res = res &&   test(var.toLongLong()==(long long)10,"MVariant(long).toLongLong()");
    res = res &&   test(var.toMString()=="10","MVariant(long).toString()");
  var = (long long)10;
    res = res &&   test(var.toLongLong()==(long long)10,"MVariant(long long).toLongLong()");
    res = res &&  test(var.toMString()=="10","MVariant(long long).toString()");
  var = MVariant(10.f);
    res = res &&  test(var.toFloat()==(float)10,"MVariant(float)");
    res = res &&  test(var.toDouble()==(double)10,"MVariant(float).toDouble()");
    res = res &&  test(var.toLongDouble()==(long double)10,"MVariant(float).toLongDouble()");
    res = res &&  test(var.toMString(2)=="10.00","MVariant(float).toString()");
  var = (double) 10;
    res = res &&   test(var.toDouble()==(double)10,"MVariant(double)");
    res = res &&  test(var.toLongDouble()==(long double)10,"MVariant(double).toLongDouble()");
    res = res &&   test(var.toMString(2)=="10.00","MVariant(double).toString()");
  var = (long double) 10;
    res = res &&   test(var.toLongDouble()==(long double)10,"MVariant(long double)");
    res = res &&  test(var.toMString(2)=="10.00","MVariant(long double).toString()");
  var = true;
    res = res &&  test(var.toBool()==true,"MVariant(bool)");
    res = res &&   test(var.toMString()=="true","MVariant(bool).toMString");
  var ="10";
  printMag(var.toMString());
    res = res &&   test(var.toMString()=="10","MVariant(MString)");
    res = res &&   test(var.toInt()==10,"MVariant(MString).toInt()");
    res = res &&   test(var.toLong()==(long)10,"MVariant(MString).toLong()");
    res = res &&   test(var.toLongLong()==(long long)10,"MVariant(MString).toLongLong()");
    res = res &&   test(var.toFloat()==(float)10,"MVariant(MString).toFloat()");
    res = res &&   test(var.toDouble()==(double)10,"MVariant(MString).toDouble()");
    res = res &&   test(var.toLongDouble()==(long double)10,"MVariant(MString).toLongDouble()");
    res = res &&   test(var.toBool()==true,"MVariant(MString).toBool()");
  int a=10;
  var = &a;
    res = res &&   test(isEqual(var.getTypeName(),"MPointer"),"MVariant(void*)");
    res = res &&   test(*((int*)var.toPointer())==10,"MVariant(void*).toPointer");
  printMag(var.toMString());
  var = MVariant();
    res = res &&   test(var.isNil(),"MVariant().isNil");
 return res;
  
}
