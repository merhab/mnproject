//
// Created by MERHAB NOUREDDINE on 06/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#pragma once
#include "MTree.h"
#include "test.h"
inline bool mTree_t(){
    printMag(
            "-----------------------------------------\n"
            "-----------TESTING MTREE--------------\n"
            "-----------------------------------------\n"
    );
   bool res = true;
    MBTree<int> tree(10);
    tree.setLeftNode(25,tree._root);
    tree.setRightNode(333,tree._root);
    printYellow("leftNode:" + MString::fromInt(tree._root->getLeftNode()->_value) + "\n");

//    tree.root = tree.newNode(10, nullptr);
//    auto root = tree.root;
//    tree.newLeftNode(10,root);
//    tree.newRightNode(45,root);
//    printRed(MString::fromInt(tree.leftNode(root)->_data)+"\n");



    return res;
}