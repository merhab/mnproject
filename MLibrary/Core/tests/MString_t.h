//
// Created by MERHAB on 30/08/2023.
//
#pragma once
#include "test.h"
#include "MString.h"
inline MString sss(MString str){
    return str;
}
inline bool mString_t(){
    printMag(
            "-----------------------------------------\n"
            "-----------TESTING MSTRING--------------\n"
            "-----------------------------------------\n"
    );
    bool res = true;
    MString str("hello");
    str = "hello";
    MString s = str;
    s[0]='m';
    res = res &&     test(isEqual(str.cstring(),"hello"),"MString str;str = \"hello\";");
    MString sssss = sss("hello");
    res = res &&     test(isEqual(sssss.cstring(),"hello"),"func param by val");
     str = "hello";
    MString str2 = str + " merhab";
    res = res &&     test(str2 == "hello merhab","MString str = str + \" merhab\";");
    str[0]='m';
    res = res &&     test(str == "mello","MString[]");

    res = res &&     test(MString("10").toInt()==10,"toInt()");
    printf("%ld",LONG_MAX);
    res = res &&     test(MString("2147483647").toLong()==2147483647,"toLong()");
    printf("%lld",LLONG_MAX);
    res = res &&     test(MString("9223372036854775807").toLongLong()==9223372036854775807,"toLongLong()");
    res = res &&     test(MString("2.1").toFloat()==2.1f,"toFloat()");
    printf("%f\n",MString("2.1").toFloat());
    res = res &&     test(MString("2.1").toDouble()==2.1,"toDouble()");
    printf("%lf\n",MString("2.1").toDouble());
    res = res &&     test(MString("2.1").toLongDouble()==2.1,"toLongDouble()");
    printf("%Lf\n",MString("2.1").toLongDouble());
    str = MString::fromInt(10);
    res = res &&     test(str == "10","MString::fromInt");
    res = res &&     test(MString::fromLong(214748364)=="214748364","fromLong()");
    res = res &&     test(MString::fromLongLong(214748364)=="214748364","fromLongLong()");
    res = res &&     test(MString::fromFloat(2.1f,3)=="2.100","fromFloat()");
    printMag(MString::fromFloat(2.1f,3));
    res = res &&     test(MString::fromDouble(2.1,3)=="2.100","fromDouble()");
    printMag(MString::fromDouble(2.1,4));
    res = res &&     test(MString::fromLongDouble(2.1,3)=="2.100","fromLongDouble()");
    printMag(MString::fromLongDouble(2.1,4));
    res = res &&     test(MString("234234.5").isNumeric(),"isNumeric(\"234234.5\")");
    res = res &&     test(!MString("2342.34.5").isNumeric(),"!isNumeric(\"2342.34.5\")");
    res = res &&     test(MString(".2342345").isNumeric(),"isNumeric(\".2342345\")");
    res = res &&     test(MString("2342345.").isNumeric(),"isNumeric(\"2342345.\")");
    res = res &&     test(MString("-2342345").isNumeric(),"!isNumeric(\"-2342345\")");
    res = res &&     test(!MString("2342-345").isNumeric(),"!isNumeric(\"2342-345\")");
    res = res &&     test(!MString("2342-34-5").isNumeric(),"!isNumeric(\"2342-34-5\")");
    res = res &&     test(!MString("2342345-").isNumeric(),"!isNumeric(\"2342345-\")");
    res = res &&     test(MString("234234.5").isNumericDecimal(),"isNumericDecimal(\"234234.5\")");
    res = res &&     test(!MString("2342345").isNumericDecimal(),"!isNumericDecimal(\"2342345\")");
    res = res &&     test(MString("-234234.5").isNumericNegative(),"isNumericNegative(\"-234234.5\")");
    res = res &&     test(!MString("-234234.5").isNumericUnsigned(),"!isNumericUnsigned(\"-234234.5\")");
    res = res &&     test(MString("2342345").isNumericUnsigned(),"isNumericUnsigned(\"2342345\")");
    res = res &&     test(MString("2342345").toMaxUint()==2342345,"(MString(\"2342345\").toMaxUint()==2342345");
    res = res &&     test(sizeof(MString("2342345").toMaxUint())==sizeof(unsigned long long int),"(sizeof(MString(\"2342345\").toMaxUint())==sizeof(unsigned long long int)");

    res = res &&    test(MString("2342345").toMaxInt()==2342345,"(MString(\"2342345\").toMaxInt()==2342345");
    res = res &&    test(sizeof(MString("2342345").toMaxInt())==sizeof(long long int),"(sizeof(MString(\"2342345\").toMaxInt())==sizeof( long long int)");


    res = res &&    test(MString("2342345.6").toMaxDouble()==2342345.6,"(MString(\"2342345.6\").toMaxDouble()==2342345.6");
    res = res &&    test(sizeof(MString("2342345").toMaxDouble())==sizeof( long double),"(sizeof(MString(\"2342345\").toMaxDouble())==sizeof(long double)");
    
    str = "hello";
    res = res &&    test(isEqual(str.getCString(), "hello"),"getCString()");
    str = "true";
    res = res &&     test(str.toBool()==true,"str.toBool(true)");
    str = "false";
    res = res &&    test(str.toBool()==false,"str.toBool(false)");
    str = "1";
    res = res &&    test(str.toBool()==true,"str.toBool(\"1\")");
    str = "0";
    res = res &&     test(str.toBool()==false,"str.toBool(\"0\")");
    return res;
}

