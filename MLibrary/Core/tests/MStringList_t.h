//
// Created by MERHAB NOUREDDINE on 28/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#pragma once

#include "MStringList.h"
#include "test.h"

inline bool mStringList_t(){
    printMag(
            "-----------------------------------------\n"
            "-----------TESTING MSTRINGLIST--------------\n"
            "-----------------------------------------\n"
    );
    bool res = true;
    MStringList list = {"hello","nour","how","are","u"};
    auto str = list.join(";");
    res = str == "hello;nour;how;are;u";
    test(res,"MStringList::join()");
    return res;
}