//
// Created by MERHAB NOUREDDINE on 25/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#pragma once

#include "MSharedPtr.h"
#include "test.h"
inline bool mSharedPointer_t(){
    printMag(
            "-----------------------------------------\n"
            "-----------TESTING MSharedPtr--------------\n"
            "-----------------------------------------\n"
    );
    bool res = true;
    MSharedPtr<int> ptr(1);
    ptr.setData(10);
    res = (ptr.getRef() == 1);
    test(res,"MSharedPtr created successfully");
    auto ptr2 = ptr;
    res = (ptr.getRef() == 2);
    test(res,"MSharedPtr copied successfully");
    printRed(MString::fromInt((ptr2.getData())));
    MSharedPtr<int> ptr3(ptr2);
    res = (ptr.getRef() == 3);
    test(res,"MSharedPtr cloned successfully");
    printRed(MString::fromInt((ptr3.getData())));
    return res;
}