//
// Created by MERHAB NOUREDDINE on 02/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#pragma once
#include <iostream>
#include "MNumbers.h"
#include "test.h"

inline bool mNumbers_t(){
    printMag(
            "-----------------------------------------\n"
            "-----------TESTING MNUMBERS--------------\n"
            "-----------------------------------------\n"
    );
    bool res = true;
    MNumber n;
   res = res && test(n.isNil(),"MNumber()");
    printMag("-----------------------------------------------\n");
    n = MNumber((unsigned char)10);
    res = res && test(n.getType() == MUChar," MNumber((unsigned char)10)");
    res = res && test(n.toUnsignedChar()==10,"n.toUnsignedChar()==10");
    res = res && test(sizeof(n.toUnsignedChar())==sizeof(unsigned char),"sizeof(n.toUnsignedChar())==sizeof(unsigned char)");
    res = res && test(n.toUnsignedLongLongInt()==10,"n.toUnsignedLongLongInt()==10");
    res = res && test(sizeof(n.toUnsignedLongLongInt())==sizeof(unsigned long long int),"sizeof(n.toUnsignedLongLongInt())==sizeof(unsigned long long int)");
    printMag("-----------------------------------------------\n");
    n = MNumber((unsigned short int)10);
    res = res && test(n.getType() == MUShortInt," MNumber((unsigned short int)10)");
    res = res && test(n.toUnsignedChar()==10,"n.toUnsignedChar()==10");
    res = res && test(sizeof(n.toUnsignedChar())==sizeof(unsigned char),"sizeof(n.toUnsignedChar())==sizeof(unsigned char)");
    res = res && test(n.toUnsignedLongLongInt()==10,"n.toUnsignedLongLongInt()==10");
    res = res && test(sizeof(n.toUnsignedLongLongInt())==sizeof(unsigned long long int),"sizeof(n.toUnsignedLongLongInt())==sizeof(unsigned long long int)");
    printMag("-----------------------------------------------\n");
    n = MNumber((unsigned int)10);
    res = res && test(n.getType() == MUInt," MNumber((unsigned int)10)");
    res = res && test(n.toUnsignedChar()==10,"n.toUnsignedChar()==10");
    res = res && test(sizeof(n.toUnsignedChar())==sizeof(unsigned char),"sizeof(n.toUnsignedChar())==sizeof(unsigned char)");
    res = res && test(n.toUnsignedLongLongInt()==10,"n.toUnsignedLongLongInt()==10");
    res = res && test(sizeof(n.toUnsignedLongLongInt())==sizeof(unsigned long long int),"sizeof(n.toUnsignedLongLongInt())==sizeof(unsigned long long int)");
    printMag("-----------------------------------------------\n");
    n = MNumber((unsigned long int)10);
    res = res && test(n.getType() == MULongInt," MNumber((unsigned long int)10)");
    res = res && test(n.toUnsignedChar()==10,"n.toUnsignedChar()==10");
    res = res && test(sizeof(n.toUnsignedChar())==sizeof(unsigned char),"sizeof(n.toUnsignedChar())==sizeof(unsigned char)");
    res = res && test(n.toUnsignedLongLongInt()==10,"n.toUnsignedLongLongInt()==10");
    res = res && test(sizeof(n.toUnsignedLongLongInt())==sizeof(unsigned long long int),"sizeof(n.toUnsignedLongLongInt())==sizeof(unsigned long long int)");
    printMag("-----------------------------------------------\n");

    n = MNumber((unsigned long long int)10);
    res = res && test(n.getType() == MULongLongInt," MNumber((unsigned long long int)10)");
    res = res && test(n.toUnsignedChar()==10,"n.toUnsignedChar()==10");
    res = res && test(sizeof(n.toUnsignedChar())==sizeof(unsigned char),"sizeof(n.toUnsignedChar())==sizeof(unsigned char)");
    res = res && test(n.toUnsignedLongLongInt()==10,"n.toUnsignedLongLongInt()==10");
    res = res && test(sizeof(n.toUnsignedLongLongInt())==sizeof(unsigned long long int),"sizeof(n.toUnsignedLongLongInt())==sizeof(unsigned long long int)");
    printMag("-----------------------------------------------\n");

    n = MNumber((char)10);
    res = res && test(n.getType() == MChar," MNumber((char)10)");
    res = res && test(n.toChar()==10,"n.toChar()==10");
    res = res && test(sizeof(n.toChar())==sizeof(char),"sizeof(n.toChar())==sizeof(char)");
    res = res && test(n.toLongLongInt()==10,"n.toLongLongInt()==10");
    res = res && test(sizeof(n.toLongLongInt())==sizeof(long long int),"sizeof(n.toLongLongInt())==sizeof(long long int)");
    printMag("-----------------------------------------------\n");
    n = MNumber((wchar_t)10);
    res = res && test(n.getType() == MLongChar," MNumber((wchar_t)10)");
    res = res && test(n.toChar()==10,"n.toChar()==10");
    res = res && test(sizeof(n.toChar())==sizeof(char),"sizeof(n.toChar())==sizeof(char)");
    res = res && test(n.toLongLongInt()==10,"n.toLongLongInt()==10");
    res = res && test(sizeof(n.toLongLongInt())==sizeof(long long int),"sizeof(n.toLongLongInt())==sizeof(long long int)");
    printMag("-----------------------------------------------\n");
    n = MNumber((short int)10);
    res = res && test(n.getType() == MShortInt," MNumber((short int)10)");
    res = res && test(n.toChar()==10,"n.toChar()==10");
    res = res && test(sizeof(n.toChar())==sizeof(char),"sizeof(n.toChar())==sizeof(char)");
    res = res && test(n.toLongLongInt()==10,"n.toLongLongInt()==10");
    res = res && test(sizeof(n.toLongLongInt())==sizeof(long long int),"sizeof(n.toLongLongInt())==sizeof(long long int)");
    printMag("-----------------------------------------------\n");
    n = MNumber((int)10);
    res = res && test(n.getType() == MInt," MNumber((int)10)");
    res = res && test(n.toChar()==10,"n.toChar()==10");
    res = res && test(sizeof(n.toChar())==sizeof(char),"sizeof(n.toChar())==sizeof(char)");
    res = res && test(n.toLongLongInt()==10,"n.toLongLongInt()==10");
    res = res && test(sizeof(n.toLongLongInt())==sizeof(long long int),"sizeof(n.toLongLongInt())==sizeof(long long int)");
    printMag("-----------------------------------------------\n");

    n = MNumber((long int)10);
    res = res && test(n.getType() == MLongInt," MNumber((long int)10)");
    res = res && test(n.toChar()==10,"n.toChar()==10");
    res = res && test(sizeof(n.toChar())==sizeof(char),"sizeof(n.toChar())==sizeof( char)");
    res = res && test(n.toLongLongInt()==10,"n.toLongLongInt()==10");
    res = res && test(sizeof(n.toLongLongInt())==sizeof(long long int),"sizeof(n.toLongLongInt())==sizeof(long long int)");
    printMag("-----------------------------------------------\n");

    n = MNumber((long long int)10);

    res = res && test(n.getType() == MLongLongInt," MNumber((long Long int)10)");
    res = res && test(n.toChar()==10,"n.toChar()==10");
    res = res && test(sizeof(n.toChar())==sizeof(char),"sizeof(n.toChar())==sizeof( char)");
    res = res && test(n.toLongLongInt()==10,"n.toLongLongInt()==10");
    res = res && test(sizeof(n.toLongLongInt())==sizeof(long long int),"sizeof(n.toLongLongInt())==sizeof(long long int)");

    printMag("-----------------------------------------------\n");

    n = MNumber((float)10.0);
    res = res && test(n.getType() == MFloat," MNumber((float)10.0)");
    res = res && test(n.toFloat()==10.0,"n.toFloat()==10.0");
    res = res && test(sizeof(n.toFloat())==sizeof(float),"sizeof(n.toFloat())==sizeof( float)");
    res = res && test(n.toLongDouble()==10.0,"n.toLongDouble()==10.0");
    res = res && test(sizeof(n.toLongDouble())==sizeof( long double),"sizeof(n.toLongDouble())==sizeof(long double)");

    printMag("-----------------------------------------------\n");

    n = MNumber((double)10.0);
    res = res && test(n.getType() == MDouble," MNumber((double)10.0)");
    res = res && test(n.toFloat()==10.0,"n.toFloat()==10.0");
    res = res && test(sizeof(n.toFloat())==sizeof(float),"sizeof(n.toFloat())==sizeof( float)");
    res = res && test(n.toLongDouble()==10.0,"n.toLongDouble()==10.0");
    res = res && test(sizeof(n.toLongDouble())==sizeof( long double),"sizeof(n.toLongDouble())==sizeof(long double)");

    printMag("-----------------------------------------------\n");

    n = MNumber((long double)10.0);
    res = res && test(n.getType() == MLongDouble," MNumber((long double)10.0)");
    res = res && test(n.toFloat()==10.0,"n.toFloat()==10.0");
    res = res && test(sizeof(n.toFloat())==sizeof(float),"sizeof(n.toFloat())==sizeof( float)");
    res = res && test(n.toLongDouble()==10.0,"n.toLongDouble()==10.0");
    res = res && test(sizeof(n.toLongDouble())==sizeof( long double),"sizeof(n.toLongDouble())==sizeof(long double)");

    printMag("-----------------------------------------------\n");

    printMag("-----------------------------------------------\n");

    n = MNumber(true);
    res = res && test(n.getType() == MBool," MNumber((bool)true)");
    res = res && test(n.toFloat()==1,"n.toFloat()==1");
    res = res && test(sizeof(n.toFloat())==sizeof(float),"sizeof(n.toFloat())==sizeof( float)");
    res = res && test(n.toLongDouble()==1,"n.toLongDouble()==1");
    res = res && test(sizeof(n.toLongDouble())==sizeof( long double),"sizeof(n.toLongDouble())==sizeof(long double)");

    printMag("-----------------------------------------------\n");


    n = MNumber(&n);
    res = res && test(n.getType() == MPointer," MNumber((pointer)&n)");
    res = res && test(n.toLongLongInt()==(long long int)&n,"n.toLongLongInt()==adr");
    res = res && test(sizeof(n.toLongLongInt())==sizeof(long long int),"sizeof(n.toLongLongInt())==sizeof(long long int)");

    printMag("-----------------------------------------------\n");
    
    n = (unsigned long long int) 100;
    res = res && test(n.getType() == MULongLongInt," MNumber((unsigned long long int)10)");

    res = res && test(n.toUnsignedChar()==100,"n.toUnsignedChar()==100");
    res = res && test(sizeof(n.toUnsignedShortInt())==sizeof(unsigned short int),"sizeof(n.toUnsignedShortInt())==sizeof(unsigned short int");

    res = res && test(n.toUnsignedInt()==100,"n.toUnsignedInt()==100");
    res = res && test(sizeof(n.toUnsignedInt())==sizeof(unsigned int),"sizeof(n.toUnsignedInt())==sizeof(unsigned int)");

    res = res && test(n.toUnsignedLongInt()==100,"n.toUnsignedLongInt()==100");
    res = res && test(sizeof(n.toUnsignedLongInt())==sizeof(unsigned long int),"sizeof(n.toUnsignedLongInt())==sizeof(unsigned long int)");

    res = res && test(n.toUnsignedLongLongInt()==100,"n.toUnsignedLongLongInt()==100");
    res = res && test(sizeof(n.toUnsignedLongLongInt())==sizeof(unsigned long long int),"sizeof(n.toUnsignedLongLongInt())==sizeof(unsigned long long int)");

    res = res && test(n.toChar()==100,"n.toChar()==100");
    res = res && test(sizeof(n.toChar())==sizeof(char),"sizeof(n.toChar())==sizeof(char)");

    res = res && test(n.toLongChar()==100,"n.toLongChar()==100");
    res = res && test(sizeof(n.toLongChar())==sizeof(wchar_t),"sizeof(n.toLongChar())==sizeof(wchar_t)");

    res = res && test(n.toShortInt()==100,"n.toShortInt()==100");
    res = res && test(sizeof(n.toShortInt())==sizeof(short int),"sizeof(n.toShortInt())==sizeof(short int)");


    res = res && test(n.toInt()==100,"n.toInt()==100");
    res = res && test(sizeof(n.toInt())==sizeof(int),"sizeof(n.toInt())==sizeof(int)");

    res = res && test(n.toLongInt()==100,"n.toLongInt()==100");
    res = res && test(sizeof(n.toLongInt())==sizeof(long int),"sizeof(n.toLongInt())==sizeof(long int)");


    res = res && test(n.toLongLongInt()==100,"n.toLongLongInt()==100");
    res = res && test(sizeof(n.toLongLongInt())==sizeof(long long int),"sizeof(n.toLongLongInt())==sizeof(long long int)");

    res = res && test(n.toFloat()==100,"n.toFloat()==100");
    res = res && test(sizeof(n.toFloat())==sizeof(float),"sizeof(n.toFloat())==sizeof(float)");


    res = res && test(n.toDouble()==100,"n.toDouble()==100");
    res = res && test(sizeof(n.toDouble())==sizeof(double),"sizeof(n.toDouble())==sizeof(double)");


    res = res && test(n.toLongDouble()==100,"n.toLongDouble()==100");
    res = res && test(sizeof(n.toLongDouble())==sizeof(long double),"sizeof(n.toLongDouble())==sizeof(long double)");


    res = res && test(n.tooBool()==true,"n.tooBool()==true");
    res = res && test(sizeof(n.tooBool())==sizeof(bool),"sizeof(n.tooBool())==sizeof(bool)");

    char str[1000];
    n.toCString(str,1000);
    res = res && test(MString(str)=="100","n.toCString()");

    n = n + 10;
    res = res && test(n.toInt()==110,"+");

    n = n - 10;
    res = res && test(n.toInt()==100,"-");

    n = n * 10;
    res = res && test(n.toInt()==1000,"*");
    
    n = n / 10;
    res = res && test(n.toInt()==100,"/");

    
    n = n % 9;
    res = res && test(n.toInt()==1,"%");

    
    n++;
    res = res && test(n.toInt()==2,"++");

    
    n--;
    res = res && test(n.toInt()==1,"--");

    
    res = res && test(n == 1,"==");

    res = res && test(n > 0,">");
    
    res = res && test(n >= 1,">=");

    res = res && test(n <= 1,"<=");

    res = res && test(n < 2,"<");

    n = &n;

    res = res && test(n.isPointer(),"isPointer()");

    res = res && test(n.getPointer()== &n,"getPointer()");
      
    return res;
}
