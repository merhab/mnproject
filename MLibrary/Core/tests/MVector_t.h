//
// Created by MERHAB NOUREDDINE on 31/08/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#pragma once
#include "MVector.h"
#include "test.h"

bool mVector_t(){
    printMag(
            "-----------------------------------------\n"
            "------------TESTING MVECTOR--------------\n"
            "-----------------------------------------\n"
    );
    bool res = true;
    MVector<int> vector(1);
    vector.append(1);
    auto v = vector;
    res = res &&     test(vector.size()==1,"size()");
    vector.append(2);
    res = res &&     test(v.size()==1 && vector.size()==2,"v.size()==1 and vector.size()==2");
    res = res &&     test(vector[0]==1,"MVector[0]==1");
    res = res &&     test(vector[1]==2,"MVector[1]==2");
    res = res &&     test(vector.count()==2,"count()");
    v[0]=3;
    res = res &&     test(vector[0]==1 && v[0]==3,"vector[0]==1 and v[0]==3");
    return res;
}