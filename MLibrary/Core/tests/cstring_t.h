//
// Created by MERHAB on 30/08/2023.
//
#pragma once
#include "CString.h"
#include "test.h"

inline bool cstring_t(){
    bool res = true;
    char* str1 = newCString("hello");
    res = res &&     test(isEqual(str1,"hello"),"testing newCString(\"hello\")");
    res = res &&     test(!isEqual("hellok","hello"),"testing isEqual()");
    char* s = (char *)newCString(250);
    copy(s,"hello");
    res = res &&     test(isEqual(s,"hello"),"testing copy()");
    deleteCstring((void**)&str1);
    deleteCstring((void**)&s);
    return res;
}



/*
//
// Created by MERHAB on 30/08/2023.
//
#pragma once

#include <cstdio>
#include <cassert>
#include <cstdlib>

//tested


extern int inc;
extern int del;
//tested

inline size_t sizestr(const char* str) {
    assert(str);
    size_t i = 0;
    for (; str[i]; ++i);
    return i;
}

static void* newstr(size_t size) {

    //printf("we created %d cstring\n",inc);
    char* string = (char*)malloc((size+1) * sizeof(char));
    return string;
}

inline void copystr(char* str1, const char* str2, size_t size = -1) {
    assert(str1 && str2);
    if (size == -1) {
        size_t i = 0;
        for (; str2[i]; i++) {
            str1[i] = str2[i];
        }
        str1[i] = 0;
    }
    else {
        size_t i = 0;
        for (; i < size; i++) {
            *str1++ = *str2++;
        }
        str1[i] = 0;
    }
}


//tested
static char* newstr(const char* str) {
    assert(str);
    size_t i = 0;
    for (; str[i]; i++);
    size_t v = sizestr(str);
    char* s = (char*)(newstr(i));
    copystr(s, str);
    return s;
}

static void* clone(const void* str) {
    return newstr((const char*)str);
}


//tested


static void dteletestr(void** strPtr) {
    del++;
    //printf("we deleted %d cstrings\n",del);
    free(*strPtr);
    *(strPtr) = nullptr;
    if (del == inc) {
        printf("\x1b[32m""all cstrings deleted succefully\n""\x1b[0m");
    }
}

inline bool cstring_t(){
    bool res = true;
    char* str1 = newstr("hello");
    //res = res &&     test(isEqual(str1,"hello"),"testing newCString(\"hello\")");
    //res = res &&     test(!isEqual("hellok","hello"),"testing isEqual()");
    char* s = (char *)newstr(250);
   copystr(s,"hello");
   /// res = res &&     test(isEqual(s,"hello"),"testing copy()");
    char* a = new char[20];
    void* b = (void*)a;
    delete b;
    char** ss = &str1;
    //delete str1;
    //delete s;
    dteletestr((void**)ss);
    //deleteCstring((void**)(ss));
    //deleteCstring((void**)(& s));
    return res;
}

*/