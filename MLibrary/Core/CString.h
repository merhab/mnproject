//
// Created by MERHAB on 30/08/2023.
//
#pragma once

#include <cstdio>
#include <cassert>
#include <cstdlib>

//tested
inline size_t size(const char* str){
    assert(str);
    size_t i=0;
    for(;str[i];++i);
    return i;
}

//tested
inline void copy(char* str1 , const char* str2,size_t size = -1){
    assert(str1 && str2);
    if (size == -1) {
        size_t i = 0;
        for (; str2[i]; i++) {
            str1[i] = str2[i];
        }
        str1[i] = 0;
    }  
    else {
        size_t i = 0;
        for (; i < size; i++) {
            str1[i] = str2[i];
        }
        str1[i] = 0;
    }

}

static int inc ;
static int del ;
//tested
inline void* newCString(size_t size){
    inc++;
    //printf("we created %d cstring\n",inc);
    char* string = (char*)malloc((size+1)*sizeof(char));
    return string;
}


//tested
inline char* newCString(const char* str){
    assert(str);
    size_t i = 0;
    for (; str[i]; ++i);
    char* s = (char*)(newCString(i));
    copy(s, str);
    return s;
}

inline void* cloneCString(const void* str){
    return newCString((const char*)str);
}


//tested
inline bool isEqual(const char* str1 , const char* str2 ){
    if(size(str1) != size(str2)) return false;
    for (size_t i =0 ;str2[i];i++){
        if(str1[i]!=str2[i]){
            return false;
        }
    }
    return true;
}

inline void deleteCstring(void** strPtr){
    del++;
    //printf("we deleted %d cstrings\n",del);
    free(*strPtr);
    *(strPtr) = nullptr;
    if (del == inc) {
        printf("\x1b[32m""all cstrings deleted succefully\n""\x1b[0m");
    }
}
