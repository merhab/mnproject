#pragma once
#include "MAlphaNumList.h"

typedef MAlphaNumList (*MEventFunc)(void*, void*, MAlphaNumList) ;
class MEvent{
public:
    MEvent(MEventFunc function, int type, MString name="");

    MString &name() ;

  void setName(const MString &name);

  int type();

  void setType(int type);

  void setFunc(MEventFunc func);

  void execFunc(void* sender,void* receiver,MAlphaNumList args ={});

  bool execCanFunc(void* sender,void* receiver,MAlphaNumList args={});

protected:
  MString _name;
  int _type;
  MEventFunc _func;
};
