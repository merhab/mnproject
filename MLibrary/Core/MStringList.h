//
// Created by MERHAB NOUREDDINE on 15/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#pragma once

#include <initializer_list>
#include "MVector.h"
#include "MString.h"

class MStringList {
    MVector<MString> _strings;
public:
    MString &operator[](size_t idx) ;

    const MString &operator[](size_t idx) const ;

    size_t count();

    size_t size();

    MStringList& append(MString val);
    MStringList& append(MStringList strList);
    MStringList(std::initializer_list<MString>);
    MStringList& append(std::initializer_list<MString>);
    MStringList() = default;

    MString join(const char *string);
    bool isEmpty();
    size_t getCharCount();
};
