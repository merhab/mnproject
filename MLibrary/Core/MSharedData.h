//
// Created by MERHAB NOUREDDINE on 01/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#pragma once

static  int datN = 0;
#include <cstddef>
#include <cassert>

typedef void* (*NewFunc)(size_t);
typedef void (*DeleteFunc)(void**);
typedef void* (*CloneFunc)(const void*);
typedef char* (*GetCStringFunc)(void*);
struct MDataFuncs{
    NewFunc _newFunc= nullptr;
    DeleteFunc _deleteFunc= nullptr;
    CloneFunc _cloneFunc = nullptr;
    GetCStringFunc _getCString = nullptr;
    MDataFuncs(NewFunc newFunc, DeleteFunc deleteFunc,CloneFunc cloneFunc )
    : _newFunc(newFunc), _deleteFunc(deleteFunc),_cloneFunc(cloneFunc) {}
    const char* _typeName = nullptr;
    MDataFuncs():MDataFuncs(nullptr, nullptr, nullptr){}
    char *getCString(void * _data) const {
        return _getCString(_data);
    }
};
class MSharedData{
protected:
    MDataFuncs _funcs;
    size_t _size = 0;
    size_t _count = 0;
    int* _ref = nullptr;
public:
    size_t getSize() const {
        return _size;
    }

    size_t getCount() const {
        return _count;
    }
    void* _data = nullptr;

    void* cloneData(void* data){
        if(!_funcs._cloneFunc){
            assert(false);
            abort();
        }
        return _funcs._cloneFunc(data);
    }
    void setData(void* data){
        _data = data;
    }
    void detach(){
        if(*_ref ==1) return;
        (*_ref)--;
        _data = cloneData(_data);
        _ref = new int;
        *_ref = 1;
    }

  /*--------------------------------------*/
  /*-------------constructors-------------*/
  /*--------------------------------------*/ 

    MSharedData(MDataFuncs funcs,size_t size): _funcs(funcs){
        _size = size;
        _count =0;
        _ref = new int;
        (*_ref) = 1;
        _data = funcs._newFunc(size);
    }

    MSharedData()=default;

  explicit MSharedData(MDataFuncs funcs,size_t size,void* data,bool willCloneData = true): _funcs(funcs)
    {
        if (willCloneData)
            _data = cloneData(data);
        else
            _data = data;
        _size = size;
        _count = size;
        _ref = new int;
        (*_ref)=1;
    }
  MSharedData(const MSharedData &data): _funcs(data._funcs)
  {
        _data = data._data;
    _size = data._size;
    _count = data._count;
    _ref = data._ref;
    (*_ref)++;
    }

  /*--------------------------------------*/
  /*-------------constructors-------------*/
  /*--------------------------------------*/
  bool isNil() {
      return _data == nullptr;
  }
MSharedData& operator=(const MSharedData& data){
    if(this == &data) return *this;
    if(this->_ref) {
        if (*(this->_ref) == 1) {
            _funcs._deleteFunc(&_data);
            delete _ref;
        } else {
            (*_ref)--;
        }
    }
      _data = data._data;
    _funcs = data._funcs;
    _size = data._size;
    _count = data._count;
    _ref = data._ref;
    (*_ref)++;
    return *this;
    }

    ~MSharedData() {
        if (_ref) {
            if ((*_ref) == 1) {
                _funcs._deleteFunc(&_data);
                delete _ref;
            } else {
                (*_ref)--;
            }
        }
    }

    const char *getTypeName() const {
        return _funcs._typeName;
    }


    char *getCString() {
        return _funcs.getCString(_data);
    }


};
