//
// Created by MERHAB NOUREDDINE on 05/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#pragma once
#include "MNumbers.h"
#include "MString.h"
#include <limits>

class MAlphaNum{
    MNumber _number;
    MString _string;
    bool _isNumber;
public:
    MAlphaNum(MNumber number);
    MAlphaNum(unsigned char uc):MAlphaNum(MNumber(uc)){}

    MAlphaNum(unsigned short int usi):MAlphaNum(MNumber(usi)){}

    MAlphaNum(unsigned int ui):MAlphaNum(MNumber(ui)){}

    MAlphaNum(unsigned long uli):MAlphaNum(MNumber(uli)){}

    MAlphaNum(unsigned long long ulli):MAlphaNum(MNumber(ulli)){}

    MAlphaNum(char c):MAlphaNum(MNumber(c)){}

    MAlphaNum(wchar_t lc):MAlphaNum(MNumber(lc)){}

    MAlphaNum(short int si):MAlphaNum(MNumber(si)){}

    MAlphaNum(int i):MAlphaNum(MNumber(i)){}

    MAlphaNum(long li):MAlphaNum(MNumber(li)){}

    MAlphaNum(long long lli):MAlphaNum(MNumber(lli)){}

    MAlphaNum(float f):MAlphaNum(MNumber(f)){}

    MAlphaNum(double d):MAlphaNum(MNumber(d)){}

    MAlphaNum(long double ld):MAlphaNum(MNumber(ld)){}

    explicit MAlphaNum(bool b):MAlphaNum(MNumber(b)){}

    MAlphaNum(void* p):MAlphaNum(MNumber(p)){}

    MAlphaNum(MString string);
    MAlphaNum(const char* cstring);
    MAlphaNum(const MAlphaNum & alphaNum);
    ~MAlphaNum(){};
    MAlphaNum& operator=(const MAlphaNum &alphaNum);
    bool isNumber() const;
    bool isMstring();
    bool isText();
    MAlphaNum operator+(MAlphaNum alphaNum);
    MAlphaNum operator-(MAlphaNum alphaNum);
    MAlphaNum operator*(MAlphaNum alphaNum);
    MAlphaNum operator/(MAlphaNum alphaNum);
    MAlphaNum operator%(MAlphaNum alphaNum);
    // prefix increment
    MAlphaNum& operator++();

    // postfix increment
    MAlphaNum operator++(int);

    // prefix decrement
    MAlphaNum& operator--();

    // postfix decrement
    MAlphaNum operator--(int);

    bool operator==(MAlphaNum alphaNum);

    bool  operator>(MAlphaNum alphaNum);

    bool  operator<(MAlphaNum alphaNum);

    bool  operator<=(MAlphaNum alphaNum);

    bool  operator>=(MAlphaNum alphaNum);

    unsigned char toUnsignedChar();

    unsigned short int toUnsignedShortInt();

    unsigned int toUnsignedInt();

    unsigned long int toUnsignedLongInt();

    unsigned long long int toUnsignedLongLongInt();

    char toChar();

    wchar_t toLongChar();

    short int toShortInt();

    int toInt();

    long int toLongInt();

    long long int toLongLongInt();

    float toFloat();

    double toDouble();

    long double toLongDouble();

    bool tooBool();

    void* toPointer();

    bool isNil();

    void toCString(char* strPtr,size_t size,int decimal=0);

    MAlphaNum();

    MNumber &getNumber();

    const MString getMString() const;

    MNumberType getNumberType(){
        return _number.getType();
    }

    MString toString(int decimal=2) ;

    bool isValid();
};
