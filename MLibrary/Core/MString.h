//
// Created by MERHAB on 30/08/2023.
//
#pragma once

#include "CString.h"
#include <cstdio>
#include <cstdlib>
#include "MSharedData.h"
#include "MNumbers.h"
#include "MErrorAble.h"
const size_t MSTRING_INIT_SIZE = 250;
class MString: public MErrorAble{
protected:
    MSharedData _sharedData;
  //char *_str = nullptr;
  size_t _size = 0;
  size_t _count = 0;
  static inline void _delete(char **str);


public:
  int id = 0;
  /*--------------------------------------*/
  /*-------------constructors-------------*/
  /*--------------------------------------*/  

  MString();

  MString(const char *str , bool willCloneData = true);//tested

  explicit MString(size_t size);//tested

  MString(const MString &string);//tested

  ~MString()=default;//tested
  /*--------------------------------------*/
  /*--------------------------------------*/
  /*--------------------------------------*/

  size_t getSize() const;//tested

  size_t getCount() const;//tested
  MString &operator=(const MString &str);//tested
  //    MString& operator=(const char* str);
  const char *cstring();//tested
    MString operator+(const MString &string);//tested
    bool operator==(const MString &string);//tested
    bool operator>(const MString &string);
    bool operator<(const MString &string);
  int toInt();//tested
  long toLong();//tested
  long long toLongLong();//tested
  float toFloat();//tested
  double toDouble();//tested
  long double toLongDouble();//tested
  static MString fromInt(int i);//tested
  static MString fromLong(long i);//tested
  static MString fromLongLong(long long i);//tested
  static MString fromFloat(float i, char decimal = 0);//tested
  static MString fromDouble(double i, char decimal = 0);//tested
  static MString fromLongDouble(long double i, char decimal = 0);//tested
  char& operator[](size_t idx);//tested
  const char& operator[](size_t idx) const ;//tested
  static MDataFuncs getMDataFuncs();//tested

  char *getCString();//tested
  bool isNumeric();//tested
  bool isNumericNegative();//tested
  bool isNumericUnsigned();//tested
  bool isNumericDecimal();//tested
  unsigned long long int toMaxUint();//tested
  long long int toMaxInt();//tested
  long double toMaxDouble();//tested

  bool toBool();//tested
  unsigned long long int getDecimalFraction();
  MString &operator+=(MString str);
  bool operator!=(MString str);
};

MString operator+(const char[],MString string);