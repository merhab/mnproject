//
// Created by MERHAB on 19/08/2023.
//
#pragma once
#include "MEvent.h"

class MEventList {
protected:
  MString _name;
  void* _receiver=0;
  void* _sender=0;
    MVector<MEvent> eventList;
  bool _isConnect = true;
public:
    MEventList(void* sender=0,void* receiver=0,MString name="");
  MString name();
    void setName(const MString &name);
    MEventList& registerEvent(MEvent event);
  void unregisterEvent(MString name);
  void runEvent(int eventType,MAlphaNumList args={});
  bool runCanEvent(int eventType,MAlphaNumList args={});
  void connect();
  void disconnect();
  void setReceiver(void* receiver);
  void setSender(void* sender);
  bool isGuiConnected();


};




