
#pragma once

#include "MVariant.h"
#include "MString.h"
typedef MString QString;
typedef MVariant QVariant;

enum MSqlType {
    Int, Int64, String, Float, Null
};

QString sqlTypeToStr(MSqlType type);

enum Priority {
    Obligatory, Complimentary, Normal
};

QString priorityToStr(Priority p);

class Meta {
public:
    QString getMasterFieldName() const;

    void setMasterFieldName(const QString masterFieldName);

    QString getMasterTableName() const;

    Meta &setMasterTableName(QString masterTableName);

    Meta(const QString &name, MSqlType type, bool primary, bool autoInc, bool unic,
         bool readOnly, Priority priority, bool generated, bool visible,
         const QVariant &defaultVal, QVariant col);

    Meta(const Meta &meta);

    Meta &setPrimary(bool isPrimary = true);

    Meta &setAutoInc(bool isAutoInc = true);

    Meta &setUnic(bool isUnic = true);

    Meta &setReadOnly(bool isReadOnly = true);

    Meta &setMendatory();

    Meta &setComplimentary();

    Meta &setNormalPriority();

    Meta &setNotGenerated();

    Meta &setGenerated();

    Meta &setInvisible();

    Meta &setVisible();

    Meta &setColumn(int column);

    Meta &setDefaultValue(QVariant value);

    QString getName();

    MSqlType getType() const;

    void setType(MSqlType type);

    bool isPrimary() const;

    bool isAutoInc() const;

    bool isUnic() const;

    bool isReadOnly() const;

    Priority getPriority() const;

    bool isMondatory();

    bool isGenerated() const;

    bool isVisible() const;

    const QVariant &getDefaultVal() const;

    const QVariant &getCol() const;

    const QVariant &getVisibleValue() const;

    QString getQtType();

    QVariant initVal();

    const QString &getCaption() const;

    Meta &setCaption(const QString &label);

private:
    QString name;
    MSqlType type;
    bool primary = false;
    bool autoInc = false;
    bool unic = false;
    bool readOnly = false;
    Priority priority = Normal;
    bool generated = true;
    bool visible = true;
    QVariant defaultVal;
    QVariant col = -1;
    QVariant visibleValue;
    QString masterTableName;
    QString masterFieldName;
    QString caption;

};

struct MetaBool {
    Meta *meta;
    bool hasError;
};

static Meta nullMeta = Meta("", Null, 0, 0, 0, 0, Normal, 0, 0, QVariant(), -1);

Meta metaInt(const QString &name);

Meta *newMetaInt(const QString &name);

Meta metaStr(const QString &name);

Meta* newMetaStr(const QString &name);

Meta metaFloat(const QString &name);

Meta* newMetaFloat(const QString &name);

Meta metaInt64(const QString &name);

Meta* newMetaInt64(const QString &name);

QString $(Meta meta);

static Meta id = metaInt64("id").setPrimary().setAutoInc().setInvisible().setColumn(0);
static Meta idMaster = metaInt64("idMaster").setInvisible().setColumn(1);
const QString ID = "id";
const QString IDMaster = "idMaster";
const int IDIndex = 0;
const int IDMasterIndex = 1;

