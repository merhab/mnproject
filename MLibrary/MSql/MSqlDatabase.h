#pragma once
#include "sqlite3.h"
#include "../Core/MVector.h"
#include "../Core/MSharedData.h"
#include "../Core/MErrorAble.h"
enum MDatabaseType{MSQLITE,MMYSQL};
class MSqlDatabase;
class MSqlQuery{
    MSqlDatabase* database;
};

class MSqlDatabase:public MErrorAble{

    int* _ref;
    MDatabaseType _type = MSQLITE;
public:
    MDatabaseType getType() const;

private:
    void* _db;
public:
    void *getDb() const;

private:
    MString _server;
    MString _dbName;
    MString _user;
    MString _pass;
    void close();
    void deref();
public:

    explicit MSqlDatabase(MString dbName,MDatabaseType type=MSQLITE,
                          MString server = "",MString user = "", MString pass = "");
    ~MSqlDatabase();
    MSqlDatabase(MSqlDatabase const & database);
    MSqlDatabase& operator=(MSqlDatabase const & database);
    bool operator==(MSqlDatabase const &database);
    static bool isThereDanglingDb();
    static bool deleteDanglingDb();
};


