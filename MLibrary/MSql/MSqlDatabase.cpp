#include "MSqlDatabase.h"


MSqlDatabase::MSqlDatabase(MString dbName,MDatabaseType type, MString server,
                           MString user, MString pass) {
    _ref = new int;
    *_ref = 1;
    this->_type = type;
    this->_dbName = dbName;
    this->_server = server;
    this->_user = user;
    this->_pass = pass;
    switch (_type) {

        case MSQLITE:{
            sqlite3 *db;
            char *zErrMsg = 0;
            int rc;

            rc = sqlite3_open(_dbName.getCString(), &db);

            if( rc ) {
                setNotPassed((MString("Can't open database: %s\n")+ sqlite3_errmsg(db)).getCString());
                fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
                assert(false);
                abort();
            } else {
                setPassed();
                printf("%s", "Opened database successfully\n");
            }
            _db = (void*) db;
        }
            break;
        case MMYSQL:
            _db= nullptr;
            assert(false);
            break;
    }
}

void MSqlDatabase::close() {
    switch (_type) {

        case MSQLITE:{
            if (!_db) {
                setPassed();
                return;
            }
            int res = sqlite3_close((sqlite3*)this->_db);
            if(res == SQLITE_OK){
                setPassed();
		printf("database closed\n");
                this->_db = nullptr;
            } else{
                setNotPassed((MString("database not closed error no ")
                +MString::fromInt(res)).getCString());
                assert(false);
            }
        }
            break;
        case MMYSQL:
            break;
    }

}


MSqlDatabase::~MSqlDatabase() {
    deref();

}

MSqlDatabase::MSqlDatabase(const MSqlDatabase &database)  : MErrorAble(database) {
    this->_ref = database._ref;
    this->_type = database._type;
    this->_db = database._db;
    this->_dbName = database._dbName;
    this->_server = database._server;
    this->_user = database._user;
    this->_pass = database._pass;
    *_ref = *_ref+1;
}

MSqlDatabase &MSqlDatabase::operator=(const MSqlDatabase &database) {
    if(this ==&database ) return *this;
    deref();
    this->_ref = database._ref;
    this->_type = database._type;
    this->_db = database._db;
    this->_dbName = database._dbName;
    this->_server = database._server;
    this->_user = database._user;
    this->_pass = database._pass;
    *_ref = *_ref+1;
    copy(this->_error, database._error);
    this->_isSuccess = database._isSuccess;
    return *this;
}
static MVector<MSqlDatabase> danglingDb;
void MSqlDatabase::deref() {
    if (!_ref) return;
    if ((*_ref)==1){
        close();
        if(isPassed()){
            delete _ref;
            _ref =0;
        } else{
            *_ref = 0;
            printRed(this->getError());
            bool found= false;
            for (size_t i = 0; i<danglingDb.count();i++){
                if (danglingDb[i]==*this){
                    found = true;
                    break;
                }
            }
            if (!found)
                danglingDb.append(*this);
            assert(false);
        }
    } else{
        *_ref = *_ref -1;
    }
}

bool MSqlDatabase::isThereDanglingDb() {
    return danglingDb.size();
}

bool MSqlDatabase::deleteDanglingDb() {
    bool success = true;
   for(size_t i=0 ; i<danglingDb.count();i++){
       danglingDb[i].deref();
       if (danglingDb[i].isPassed()){
       } else{
           success =  false;
       }
   }
    if (success){
        danglingDb.removeAll();
    }
    return success;
}

bool MSqlDatabase::operator==(const MSqlDatabase &database) {
    return this->_db == database._db;
}

MDatabaseType MSqlDatabase::getType() const {
    return _type;
}

void *MSqlDatabase::getDb() const {
    return _db;
}


