//
// Created by MERHAB NOUREDDINE on 11/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#include "MSqlStatment.h"


MSqlStatement::MSqlStatement(MSqlDatabase *database, MString sql) {
    _database = database;
    switch (database->getType()) {

        case MSQLITE: {
            sqlite3_stmt *rds;
            const char *tail;
            int res;
            auto *db = (sqlite3 *) database->getDb();
            res = sqlite3_prepare_v2(db, sql.getCString(),
                                     -1, &rds, &tail);
            if (!(res == SQLITE_OK)) {
                setNotPassed((MString("SQL error: %s\n") +
                              sqlite3_errmsg(db)).getCString());
                fprintf(stderr, "SQL error: %s\n", sqlite3_errmsg(db));

            } else {
                setPassed();
                this->_stmt= MSharedPtr<void *>((void *) rds);
            }
        }
            break;
        case MMYSQL:
            break;
    }
}

void MSqlStatement::finalize() {
    switch (_database->getType()) {

        case MSQLITE: {
            sqlite3_finalize((sqlite3_stmt *) this->_stmt._ptr);
//            if (errCode !=SQLITE_OK){
//                setNotPassed(((MString("can`t free the statment sqlite error no:")+
//                MString::fromInt(errCode)).getCString()));
//                assert(false);
//            } else{
//                setPassed();
//            }
        }
            break;
        case MMYSQL:
            break;
    }
}

void MSqlStatement::reset() {
    switch (_database->getType()) {
        case MSQLITE: {
            sqlite3_reset((sqlite3_stmt *) this->_stmt._ptr);
//            if (errCode !=SQLITE_OK){
//                setNotPassed(((MString("can`t free the statment sqlite error no:")+
//                               MString::fromInt(errCode)).getCString()));
//                assert(false);
//            } else{
//                setPassed();
//            }
//        }
            break;
            case MMYSQL:
                break;
        }
    }
}

void MSqlStatement::bind(MAlphaNum value, int index) {
    index = index + 1;//sqlite colomn start from 1
    switch (_database->getType()) {
        case MSQLITE: {
            auto rds = (sqlite3_stmt *) this->_stmt._ptr;
            int res;
            if (value.isNumber()) {
                switch (value.getNumberType()) {
                    case MUChar:
                    case MUShortInt:
                    case MUInt:
                    case MULongInt:
                    case MULongLongInt:
                    case MChar:
                    case MLongChar:
                    case MShortInt:
                    case MBool:
                    case MInt:
                        res = sqlite3_bind_int(rds, index, value.getNumber().toInt());
                        break;
                    case MLongInt:
                    case MLongLongInt:
                        res = sqlite3_bind_int64(rds, index, value.getNumber().toLongLongInt());
                        break;
                    case MFloat:
                    case MDouble:
                    case MLongDouble:
                        if (value.getNumber().isDecimal()) {
                            double d = value.getNumber().toDouble();
                            sqlite3_bind_double(rds, index, d);
                        }
                        break;

                    case MNil:
                        res = sqlite3_bind_null(rds, index);
                        break;
                     default:
                        assert(false);
                        break;
                }
            } else {
                auto str = value.getMString();
                res = sqlite3_bind_text(rds, index, str.getCString(), -1, nullptr);
            }

        }

            break;
        case MMYSQL:
            break;
    }
}

void MSqlStatement::exec() {
    switch (_database->getType()) {

        case MSQLITE: {
            auto rds = (sqlite3_stmt *) this->_stmt._ptr;
            int res;
            res = sqlite3_step(rds);
            if (res != SQLITE_DONE) {
                setNotPassed(((MString("can`t execute the statment sqlite error no:") +
                               MString::fromInt(res)).getCString()));
                assert(false);
            } else {
                setPassed();
            }

        }
            break;
        case MMYSQL:
            break;
    }
}

void MSqlStatement::bind(MVector<MAlphaNum> values) {
    for (int i = 0; i < values.count(); ++i) {
        bind(values[i], i);
    }
}

int MSqlStatement::colCount() {
    switch (_database->getType()) {
        case MSQLITE:
            return sqlite3_column_count((sqlite3_stmt *) this->_stmt._ptr);
            break;
        case MMYSQL:
            return 0;
            break;
    }
}

MVector<MAlphaNumList> MSqlStatement::query() {
    MVector<MAlphaNumList> dataset;
    switch (_database->getType()) {

        case MSQLITE: {
            auto stmt = (sqlite3_stmt *) this->_stmt.getData();
            int columnCount = colCount();
            while (sqlite3_step(stmt) == SQLITE_ROW) {
                dataset.append(getRow(stmt, columnCount));
            }
        }
            break;
        case MMYSQL:
            break;
    }
    return dataset;
}

MAlphaNumList MSqlStatement::getRow(sqlite3_stmt* stmt , int col_count) {
    MAlphaNumList list;
    switch (_database->getType()) {
        case MSQLITE: {
            for (int i = 0; i < col_count; i++) {
                if (sqlite3_column_type(stmt, i) == SQLITE_INTEGER) {
                    list.append(sqlite3_column_int64(stmt, i));
                    continue;
                } else if (sqlite3_column_type(stmt, i) == SQLITE_TEXT) {
                    list.append((char *) sqlite3_column_text(stmt, i));
                    continue;
                } else if (sqlite3_column_type(stmt, i) == SQLITE_FLOAT) {
                    list.append(sqlite3_column_double(stmt, i));
                    continue;
                }
            }
        }

            break;
        case MMYSQL:
            break;
    }
    return list;
}

sqlite3_stmt *MSqlStatement::getSQliteStmt() {
    return (sqlite3_stmt *) _stmt.getPointer();
}

MSqlStatement::~MSqlStatement() {
    switch (_database->getType()) {

        case MSQLITE:{
            if (_stmt.isReadyToBeDeleted()){
                auto st = (sqlite3_stmt *) this->_stmt._ptr;
                sqlite3_finalize(st);
                this->_stmt._ptr= nullptr;
            }
        }
            break;
        case MMYSQL:
            break;
    }
}

MSqlStatement::MSqlStatement(const MSqlStatement &stmt)  : MErrorAble(stmt) {
    _stmt = stmt._stmt;
    _database = stmt._database;
}

MSqlStatement &MSqlStatement::operator=(const MSqlStatement &stmt) {
    if(this == &stmt) return *this;
    switch (_database->getType()) {

        case MSQLITE:{
            if (_stmt.isReadyToBeDeleted()){
                auto s = (sqlite3_stmt *) this->_stmt._ptr;
                if (s) {
                    sqlite3_finalize(s);
                    this->_stmt._ptr = nullptr;
                }
            }
        }
            break;
        case MMYSQL:
            break;
    }
    _stmt = stmt._stmt;
    _database = stmt._database;
    copy(_error , stmt._error);
    _isSuccess = stmt._isSuccess;
    return *this;
}





