cmake_minimum_required(VERSION 3.14)

project(MSqlite LANGUAGES C)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(PROJECT_SOURCES
        sqlite3.c
        sqlite3.h
        )
        add_library(${PROJECT_NAME} STATIC
        ${PROJECT_SOURCES}
        )

target_compile_definitions(${PROJECT_NAME} PRIVATE MSQLITE_LIBRARY)
target_include_directories(${PROJECT_NAME} PUBLIC ./)

