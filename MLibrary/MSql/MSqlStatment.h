//
// Created by MERHAB NOUREDDINE on 11/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#pragma once
#include "MSqlDatabase.h"
#include "../Core/MAlphaNumList.h"

class MSqlStatement: public MErrorAble{
    MSqlDatabase* _database;
     MSharedPtr<void*> _stmt;
     void finalize();
public:
    void reset();
    MSqlStatement(MSqlDatabase* database,MString sql);
    void bind(MAlphaNum value,int index);
    void bind(MVector<MAlphaNum>values);
    void exec();
    int colCount();
    MVector<MAlphaNumList> query();
    MAlphaNumList getRow(sqlite3_stmt* stmt , int col_count);
    sqlite3_stmt* getSQliteStmt();
    ~MSqlStatement();
    MSqlStatement(const MSqlStatement &stmt);
    MSqlStatement& operator=(const MSqlStatement &stmt);

};
