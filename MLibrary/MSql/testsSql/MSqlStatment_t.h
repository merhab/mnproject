#pragma once
#include "MSqlDatabase.h"
#include "MSqlStatment.h"
#include "../Core/test.h"

#include <platform_folders.h>
#include <cwalk.h>


inline bool mSqlStatment_t(){
  bool res =true;
  char buffer[FILENAME_MAX];
  cwk_path_join(sago::getDocumentsFolder().data(), "/hello.db", buffer, sizeof(buffer));
  MString s = buffer;
  MSqlDatabase db(s);
  res = res && db.isPassed();
  MSqlStatement stmt(&db,"create table if not exists Persons (PersonID int,LastName varchar(255),FirstName varchar(255),Address varchar(255),City varchar(255));");
  res = res && stmt.isPassed();
  stmt.exec();
  res = res && stmt.isPassed();
  MString str ="INSERT INTO Persons(PersonID,LastName,FirstName,Address,City) VALUES (1,'Merhab','Noureddine','Zone 8','Mascara');";
  stmt = MSqlStatement(&db,str);
  stmt.exec();
  res = res && stmt.isPassed();  
  
  return res;
}
