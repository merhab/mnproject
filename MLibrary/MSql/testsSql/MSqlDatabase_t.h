//
// Created by MERHAB NOUREDDINE on 15/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#pragma once
#include "MSqlDatabase.h"
#include "../Core/test.h"

inline bool mSqlDatabase_t(){
    bool res = true;
    MSqlDatabase db("test1111.sqlite");
    res = db.isPassed();
    auto db2 = db;
    MSqlDatabase::deleteDanglingDb();
    return res;
}
