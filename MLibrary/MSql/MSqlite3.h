//
// Created by MERHAB NOUREDDINE on 11/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#pragma once

#include <cstdio>
#include "sqlite3.h"
#include "../Core/MAlphaNum.h"
#include "../Core/MVector.h"
typedef MVector<MAlphaNum> MAlphaNumList;
bool bind(sqlite3 *sqlitedb,char* sql,MAlphaNumList vals) {
    sqlite3_stmt *rds;
    const char *tail;
    int res;
    res = sqlite3_prepare_v2(sqlitedb, sql, -1, &rds, &tail);
    if (!(res == SQLITE_OK)) {
        fprintf(stderr, "SQL error: %s\n", sqlite3_errmsg(sqlitedb));
        sqlite3_finalize(rds);
        return 0;
    }
    int j = 1;
    for (size_t i = 0; i < vals.count(); i++) {
        auto value = vals[i];
        const char *text;
        if (value.isNumber()){
        switch (value.getNumberType()) {
            case MUChar:
            case MUShortInt:
            case MUInt:
            case MULongInt:
            case MULongLongInt:
            case MChar:
            case MLongChar:
            case MShortInt:
            case MBool:
            case MInt:
                res = sqlite3_bind_int(rds, j, value.getNumber().toInt());
                break;
            case MLongInt:
            case MLongLongInt:
                res = sqlite3_bind_int64(rds,j,value.getNumber().toLongLongInt());
                break;
            case MFloat:
            case MDouble:
            case MLongDouble:
                if (value.getNumber().isDecimal()){
                    double d = value.getNumber().toDouble();
                    sqlite3_bind_double(rds, j, d);
                }
                break;

            case MNil:
                res = sqlite3_bind_null(rds,j);
                break;
            default:
                assert(false);
            }
        }else{
            auto str = value.getMString();
            res = sqlite3_bind_text(rds, j, str.getCString(), -1, nullptr);
        }
        j++;
    }
    res = sqlite3_step(rds);
    if (res == SQLITE_OK) {
        sqlite3_finalize(rds);
        return true;
    }
    sqlite3_finalize(rds);
    return false;
}

bool exec(sqlite3_stmt* stmt){
    int res = sqlite3_step(stmt);
    if (res == SQLITE_OK) {
        sqlite3_finalize(stmt);
        return true;
    }
    sqlite3_finalize(stmt);
    return false;
}

int free(sqlite3_stmt* stmt){
    return sqlite3_finalize(stmt);
}
