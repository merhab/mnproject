#pragma once
#include "MVector.h"
#include "MString.h"
#include "MVariant.h"
typedef MVariant QVariant;
typedef MString QString;
typedef MVector QVector;
typedef MVector<MVariant> QVariantList;
typedef MVector<MString> QStringList;
typedef MVector QList;
