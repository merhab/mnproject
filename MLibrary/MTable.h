//
// Created by MERHAB on 02/08/2023.
//
#pragma once
#include "MEventList.h"
#include "MRecord.h"
#include "MRecordSet.h"
#include "MStmt.h"
#include "MStmtList.h"
#include "MetaList.h"
#include "mdataset.h"
// #include <QSqlDatabase>
// #include <QSqlError>
// #include <QSqlQuery>
// #include "MEventAble.h"


enum SqlResultType { SqlRecordSet, SqlCommand, SqlError };
struct SqlResult {
  SqlResultType type = SqlRecordSet;
  QVector<MRecord> recordset = {};
  QString error;
};
enum MTableEventType {
  MTableBeforeMove,
  MTableCanMove,
  MTableAfterMove,
  MTableBeforeUpdate,
  MTableCanUpdate,
  MTableAfterUpdate,
  MTableBeforeRemove,
  MTableCanRemove,
  MTableAfterRemove,
  MTableBeforeInsert,
  MTableCanInsert,
  MTableAfterInsert
};
class MTable: public MEventAble {
protected:
  QVector<MStmtList> stmtsList;
  QString _tableName;
  int64_t idMaster = -1;
  MetaList metaList;
  QSqlDatabase *db;
  QStringList names = {};
  MRecordSet _recordset;

public:
  MRecordSet &recordset();
  MDataSet<MRecord> &dataSet();
  MRecord &current();

  void setIdMaster(int64_t idMaster);

  const QVector<MStmtList> &getStmts() const;

  const QString &tableName() const;

  int64_t getIdMaster() const;

  MetaList &getMetaList();
  MTable &select(QVector<Meta *> metas = {});
  MTable &filter(MStmt filter, MStmtKind kind = AndFilter);
  MTable &orFilter(MStmt filter);
  MTable &andFilter(MStmt filter);
  MStmtListBool getLastSTmts();
  MTable(QSqlDatabase *db, QString tableName, QVector<Meta*> metaList);
  SqlResult exec();
  void cancel(MRecord *record);
  SqlResult exec(QString sql, QVector<Meta *> metaList,
                 QVector<QVariant> bindingVals = {});
  bool append(QVariantList vars);
  void append();
  bool insert(MRecord *record);
  bool update(MRecord *record);
  bool remove(MRecord *record);
  bool insert();
  bool update();
  bool remove();
  bool first();
  bool next();
  bool prior();
  bool last();
  bool goTo(int64_t index);

  int64_t index();
};
